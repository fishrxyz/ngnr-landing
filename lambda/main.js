const aws = require("aws-sdk");
const ddb = new aws.DynamoDB({ apiVersion: "2012-10-08" });
const ses = new aws.SES({ apiVersion: "2010-12-01" });
const cognito = new aws.CognitoIdentityServiceProvider();

const sendTemplatedEmail = async (to, username) => {
  const emailParams = {
    Source: process.env.FROM_EMAIL,
    Template: "welcome",
    Destination: {
      ToAddresses: [to],
    },
    TemplateData: `{ "username": ${username} }`,
  };

  try {
    await ses.sendTemplatedEmail(emailParams).promise();
    console.log("EMAIL SENT");
  } catch (err) {
    console.log(err);
  }
};

const sendNotification = async (body) => {
  const eParams = {
    Destination: {
      ToAddresses: ["karim.cheurfi@gmail.com"],
    },
    Message: {
      Body: {
        Text: {
          Charset: "UTF-8",
          Data: body,
        },
      },
      Subject: {
        Charset: "UTF-8",
        Data: "Woohoo ! Somebody signed up for ngnr.club",
      },
    },
    // Replace source_email with your SES validated email address
    Source: "ngnr.club@gmail.com",
  };
  try {
    await ses.sendEmail(eParams).promise();
    console.log("EMAIL SENT");
  } catch (err) {
    console.log(err);
  }
};

exports.handler = async (event, context) => {
  const poolId = event.userPoolId;
  let tableName;
  if (poolId === process.env.PROD_POOL_ID) {
    tableName = "ngnr-club-prod";
  } else if (poolId === process.env.STAGING_POOL_ID) {
    tableName = "ngnr-club-staging";
  }

  if (event.request.userAttributes.sub) {
    // NOTE: set the preferred_username attribute on the confirmed account to event.Username
    // by default
    try {
      const params = {
        UserPoolId: poolId,
      };
      const result = await cognito.listUsers(params).promise();
      const filteredResult = result.Users.filter((user) => {
        return user.Username == event.userName;
      });
      if (filteredResult.length !== 0) {
        const newUser = filteredResult[0];
        let updatedParams = {
          UserAttributes: [
            {
              Name: "preferred_username", // name of attribute
              Value: newUser.Username, // the new attribute value
            },
          ],
          UserPoolId: poolId,
          Username: newUser.Username,
        };
        await cognito.adminUpdateUserAttributes(updatedParams).promise();
      }
    } catch (error) {
      console.error(error);
      context.done(null, event);
    }
    const NewUserParams = {
      TableName: tableName,
      Item: {
        sub: { S: event.request.userAttributes.sub },
        email: { S: event.request.userAttributes.email },
        username: { S: event.userName },
        projects: { L: [] },
        cv: { M: {} },
        notifications: {
          M: {
            community: { BOOL: false },
            jobs: { BOOL: true },
            product_updates: { BOOL: true },
          },
        },
      },
    };

    try {
      await ddb.putItem(NewUserParams).promise();
      await sendTemplatedEmail(
        event.request.userAttributes.email,
        event.userName,
      );

      const body = "We have a new user ! Their name is " + event.userName;
      await sendNotification(body);
      console.log("email sent!");
    } catch (error) {
      console.log(error);
    }
    context.done(null, event);

    context.done(null, event);
  } else {
    console.log("error: nothing was written to ddb");
    context.done(null, event);
  }
};
