## 1. Create the basic exec role

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
```

then run

```bash
aws iam create-role --role-name ngnr-cognito-trigger --assume-role-policy-document file://policy.json

# attach AWSLambdaBasicExecutionRole policy to the role
aws iam attach-role-policy --role-name ngnr-cognito-trigger --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
```

## 2. Dynamo DB security

```sh
aws iam create-policy --policy-name update-ngnr-table --policy-document file://dynamodb-policy.json
aws iam attach-role-policy --role-name ngnr-cognito-trigger --policy-arn <policy-arn>
```

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "dynamodb:PutItem",
      "Resource": ["<ARN of the db>"]
    }
  ]
}
```

## 3. Cognito Policy

```bash
aws lambda add-permission --function-name ngnr-cognito-trigger --principal cognito-idp.amazonaws.com --statement-id blablaqwe --action lambda:InvokeFunction# 3. Cognito Policy
```

create policy x attach to the role

## Create the SES policy

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": ["ses:SendEmail", "ses:SendRawEmail"],
      "Resource": "*"
    }
  ]
}
```

# 4. populate environment.json with the correct values

```json
{
  "Variables": {
    "STAGING_POOL_ID": "us-east-1_4bo7Tg21F",
    "PROD_POOL_ID": "us-east-1_p2xUWlB8Z"
  }
}
```

# 5. deploy

```sh
make create
# or
make update
```

# 6. test (in the console)
