### TO-DO

### DONE

- [x] install react helmet (update favicon x title)
- [x] add link to live demo
- [x] public page (do a fake one for now) + responsify
- [x] Install react-router-dom
- [x] mobile responsive
- [x] add coming soon ting
- [x] footer
- [x] update favicon
- [x] finish the banner section
- [x] code features section
- [x] update logo on Canva
- [x] setup sass
- [x] code blurb section
- [x] add GIF section
- [x] code how to use section
