import { Helmet } from "react-helmet-async";
import Header from "../../components/faq/Header";
import Questions from "../../components/faq/Questions";
import LogoAlt from "../../assets/LogoAlt";
import { NavLink } from "react-router-dom";

const FAQPage = () => {
  return (
    <>
      <Helmet>
        <title>Frequently Asked Questions</title>
        <meta
          name="description"
          content="Everything you need to know to start using ngnr.club."
        />
      </Helmet>
      <main className="ngnr__faq">
        <div className="container">
          <Header />
          <Questions />
          <footer className="ngnr__faq__footer">
            <NavLink to={"/"}>
              <LogoAlt />
            </NavLink>
          </footer>
        </div>
      </main>
    </>
  );
};

export default FAQPage;
