import SEO from "../../components/hoc/SEO";
import Banner from "../../components/home/Banner";
import Blurb from "../../components/home/Blurb";
import CheckProfile from "../../components/home/CheckProfile";
import ComingSoonBanner from "../../components/home/ComingSoonBanner";
import DontNeed from "../../components/home/DontNeed";
import Features from "../../components/home/Features";
import Footer from "../../components/home/Footer";
import Gif from "../../components/home/Gif";
import Hero from "../../components/home/Hero";
import HowItWorks from "../../components/home/HowItWorks";
import LiveDemo from "../../components/home/LiveDemo";

import "../../styles/home/index.sass";

const HomePage = () => {
  return (
    <main id="home">
      <SEO />
      <div className="home__container">
        <ComingSoonBanner />
        <Hero />
        <Gif />
        <Blurb />
        <CheckProfile />
        <LiveDemo />
        <HowItWorks />
        <Features />
        <DontNeed />
        <Banner />
        <Footer />
      </div>
    </main>
  );
};

export default HomePage;
