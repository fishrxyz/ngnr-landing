import { RefreshDouble } from "iconoir-react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Notification from "../../components/common/Notification";
import WrapperSEO from "../../components/hoc/SEO";
import { forgotPassword } from "../../helpers/auth";

const ForgotPassword = () => {
  const [username, setUsername] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    setIsSubmitting(true);
    e.preventDefault();
    setError("");

    try {
      await forgotPassword(username);
      setSuccess(true);
    } catch (err) {
      console.log(error.name);
      setError({ msg: err.message });
    }

    setIsSubmitting(false);
  };

  useEffect(() => {
    if (success) {
      const msg = "Check your email for the code";
      navigate("/auth/reset-password", {
        replace: true,
        state: { username, msg },
      });
    }
  }, [navigate, success, username]);

  return (
    <section className="forgot-password-page">
      <WrapperSEO title={"Forgot Password"} />
      <header className="auth-page__header">
        <h2>Forgot your password ?</h2>
        <p>
          Put your username below and we'll send you a confirmation code to
          reset your password.
        </p>
      </header>

      <div className="auth__form__wrapper">
        {error && <Notification fixed error message={error.msg} />}
        <form onSubmit={handleSubmit} className="auth__form login-form">
          <div className="form__field">
            <label htmlFor="username" className="field__label">
              <span className="field__label__text">Username</span>
              <input
                type="text"
                name="username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                className="field__input"
              />
            </label>
          </div>
          <button
            className={`auth__form__submit ${isSubmitting ? "submitting" : ""}`}
            type="submit"
          >
            <p className="auth__form__submit__text">
              {isSubmitting ? (
                <>
                  <span>Sending ...</span>
                  <i>
                    <RefreshDouble width="13" height="13" strokeWidth="3" />
                  </i>
                </>
              ) : (
                <>
                  <span>Send me the code !</span>
                </>
              )}
            </p>
          </button>
        </form>
      </div>
    </section>
  );
};

export default ForgotPassword;
