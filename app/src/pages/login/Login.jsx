import { useLocation } from "react-router-dom";
import Notification from "../../components/common/Notification.jsx";
import SEO from "../../components/hoc/SEO.jsx";
import LoginForm from "../../components/login/Form";

const Login = () => {
  window.history.replaceState({}, document.title);
  const location = useLocation();

  return (
    <section className="login-page">
      <SEO title={"Login"} />
      <header className="auth-page__header">
        <h2>Welcome Back !</h2>
        {location.state?.message && (
          <Notification fixed message={location.state.message} />
        )}
      </header>

      <div className="auth__form__wrapper">
        <LoginForm />
      </div>
    </section>
  );
};

export default Login;
