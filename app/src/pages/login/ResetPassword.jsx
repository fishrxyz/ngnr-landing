import { RefreshDouble } from "iconoir-react";
import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import Notification from "../../components/common/Notification";
import WrapperSEO from "../../components/hoc/SEO";
import { confirmResetPassword } from "../../helpers/auth";

const ResetPassword = () => {
  window.history.replaceState({}, document.title);
  const location = useLocation();
  const [username, setUsername] = useState(location.state?.username || "");
  const [confirmationCode, setConfirmationCode] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [error, setError] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [success, setSuccess] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    if (success) {
      navigate("/auth/login", {
        state: { message: "Password reset successfully !" },
      });
    }
  }, [navigate, success]);

  const handleSubmit = async (e) => {
    setIsSubmitting(true);
    e.preventDefault();
    setError("");

    try {
      await confirmResetPassword(username, confirmationCode, newPassword);
      setSuccess(true);
    } catch (err) {
      console.log(error.name);
      setError({ msg: err.message });
    }

    setIsSubmitting(false);
  };

  return (
    <section className="reset-password-page">
      <WrapperSEO title={"Reset your password"} />
      <header className="auth-page__header">
        <h2>Reset your password</h2>
        <p>Use the form below to create a new password for your account.</p>
        {location.state?.msg && (
          <Notification fixed message={location.state.msg} />
        )}
      </header>

      <div className="auth__form__wrapper">
        {error && <Notification fixed error message={error.msg} />}
        <form onSubmit={handleSubmit} className="auth__form login-form">
          <div className="form__field">
            <label htmlFor="username" className="field__label">
              <span className="field__label__text">Username</span>
              <input
                type="text"
                name="username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                className="field__input"
              />
            </label>
          </div>
          <div className="form__field">
            <label htmlFor="confirmation_code" className="field__label">
              <span className="field__label__text">Confirmation Code</span>
              <input
                type="text"
                name="confirmation_code"
                value={confirmationCode}
                onChange={(e) => setConfirmationCode(e.target.value)}
                className="field__input"
              />
            </label>
          </div>
          <div className="form__field">
            <label htmlFor="new_password" className="field__label">
              <span className="field__label__text">New Password</span>
              <input
                type="password"
                name="new_password"
                value={newPassword}
                onChange={(e) => setNewPassword(e.target.value)}
                className="field__input"
              />
            </label>
          </div>
          <button
            className={`auth__form__submit ${isSubmitting ? "submitting" : ""}`}
            type="submit"
          >
            <p className="auth__form__submit__text">
              {isSubmitting ? (
                <>
                  <span>Sending ...</span>
                  <i>
                    <RefreshDouble width="13" height="13" strokeWidth="3" />
                  </i>
                </>
              ) : (
                <>
                  <span>Reset my password !</span>
                </>
              )}
            </p>
          </button>
        </form>
      </div>
    </section>
  );
};

export default ResetPassword;
