import SEO from "../../components/hoc/SEO";
import RegisterForm from "../../components/register/RegisterForm";

const Register = () => {
  return (
    <>
      <SEO title={"Create an account"} />
      <section className="register-page">
        <header className="auth-page__header">
          <h2>Create Account</h2>
        </header>

        <div className="auth__form__wrapper">
          <RegisterForm />
        </div>
      </section>
    </>
  );
};

export default Register;
