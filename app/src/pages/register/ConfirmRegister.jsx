import { useLocation } from "react-router-dom";
import SEO from "../../components/hoc/SEO";
import ConfirmRegisterForm from "../../components/register/ConfirmRegisterForm";

const ConfirmRegister = () => {
  const location = useLocation();
  window.history.replaceState({}, document.title);

  return (
    <>
      <SEO title={"Confirm your account"} />
      <section className="confirm-register-page">
        <header className="auth-page__header">
          {location.state?.email ? (
            <>
              <h2>Please check your email</h2>
              <p>
                We&apos;ve sent a confirmation code to{" "}
                <b>{location.state.email}</b>
              </p>
            </>
          ) : (
            <h2>Confirm your account</h2>
          )}
        </header>

        <div className="auth__form__wrapper">
          <ConfirmRegisterForm />
        </div>
      </section>
    </>
  );
};

export default ConfirmRegister;
