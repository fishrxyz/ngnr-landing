import { Link } from "react-router-dom";
import NotFoundGif from "../../assets/404.gif";
import LogoAlt from "../../assets/LogoAlt";
import SEO from "../../components/hoc/SEO";

const NotFound = () => {
  return (
    <>
      <SEO title={"Page Not Found"} />
      <main id="not-found">
        <div className="not-found__container">
          <section className="not-found__content">
            <figure className="not-found__img">
              <img src={NotFoundGif} alt="not-found" />
            </figure>
            <article className="not-found__text">
              <h2 className="not-found__title">Page not found</h2>
              <p className="not-found__subtitle">
                The page you were looking for does not exist or has been moved.
              </p>
              <Link className="not-found__link" to="/">
                Go Home ?
              </Link>
            </article>
          </section>
          <footer className="not-found__footer">
            <div className="not-found__logo-container">
              <LogoAlt />
            </div>
          </footer>
        </div>
      </main>
    </>
  );
};

export default NotFound;
