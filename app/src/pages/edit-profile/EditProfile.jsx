import { useContext } from "react";
import LoadingSpinner from "../../components/common/LoadingSpinner";
import ContactForm from "../../components/edit-profile/ContactForm";
import InfoForm from "../../components/edit-profile/InfoForm";
import SocialForm from "../../components/edit-profile/SocialForm";
import Header from "../../components/edit-profile/Header";
import Nav from "../../components/edit-profile/Nav";
import Error from "../../components/hoc/Error";
import SEO from "../../components/hoc/SEO";
import { ProfileContext } from "../../context/ProfileContext";
import Notification from "../../components/common/Notification";
import ProjectsForm from "../../components/edit-profile/ProjectsForm";
import WorkPreferencesForm from "../../components/edit-profile/WorkPreferencesForm";

const EditProfile = () => {
  const { error, profileLoading, submitError, submitSuccess } =
    useContext(ProfileContext);

  if (profileLoading) {
    return <LoadingSpinner message={"Fetching profile info ..."} />;
  } else {
    if (error) {
      return <Error status={error.status} />;
    } else {
      return (
        <>
          <SEO title={"Your Profile"} />
          <main className="protected__page">
            <div className="protected__container">
              {submitSuccess && <Notification mini message="Changes saved !" />}
              {submitError && (
                <Notification mini error message={submitError.msg} />
              )}
              <Header />
              <div className="protected__form__container">
                <Nav />
                <InfoForm />
                <WorkPreferencesForm />
                <SocialForm />
                <ContactForm />
                <ProjectsForm />
              </div>
            </div>
          </main>
        </>
      );
    }
  }
};

export default EditProfile;
