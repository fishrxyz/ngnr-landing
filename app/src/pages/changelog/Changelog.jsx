import ReleaseNotesGif from "../../assets/release-notes.gif";
import WrapperSEO from "../../components/hoc/SEO";
import Change from "../../components/changelog/Change";
import changes from "../../data/changelog.json";

const Changelog = () => {
  return (
    <>
      <WrapperSEO
        title={"Release Notes"}
        description={"Latest releases from the ngnr.club team"}
      />
      <section className="ngnr__changelog">
        <div className="ngnr__changelog__container">
          <header className="ngnr__changelog__header">
            <h2>Release Notes</h2>
            <p>Product updates and announcements from the ngnr.club team</p>
          </header>
          <figure className="ngnr__changelog__image">
            <img src={ReleaseNotesGif} alt="release notes" />
          </figure>
          <div className="ngnr__changelog__changes">
            {changes.map((change, idx) => (
              <Change key={idx} change={change} />
            ))}
          </div>
        </div>
      </section>
    </>
  );
};

export default Changelog;
