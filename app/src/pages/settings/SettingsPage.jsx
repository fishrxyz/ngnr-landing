import { useContext } from "react";
import LoadingSpinner from "../../components/common/LoadingSpinner";
import Notification from "../../components/common/Notification";
import Error from "../../components/hoc/Error";
import WrapperSEO from "../../components/hoc/SEO";
import DataExport from "../../components/settings/DataExport";
import DeleteAccountForm from "../../components/settings/DeleteAccountForm";
import Header from "../../components/settings/Header";
import Nav from "../../components/settings/Nav";
import NotificationsPreferences from "../../components/settings/NotificationsPreferences";
import UpdatePasswordForm from "../../components/settings/UpdatePasswordForm";
import UpdateUsernameForm from "../../components/settings/UpdateUsernameForm";
import { ProfileContext } from "../../context/ProfileContext";

const SettingsPage = () => {
  const { profileLoading, error, deleteAccount, submitSuccess, submitError } =
    useContext(ProfileContext);

  if (profileLoading) {
    return <LoadingSpinner message={"Loading settings ..."} />;
  } else {
    if (error) {
      return <Error status={error.status} />;
    } else {
      return (
        <>
          {submitSuccess && <Notification mini message="Changes saved !" />}
          {submitError && (
            <Notification mini error message="An error occured" />
          )}
          <WrapperSEO title={"Settings"} />
          <main id="ngnr__settings" className="protected__page">
            <div className="ngnr__settings__container protected__container">
              <Header />
              <div className="protected__form__container">
                <Nav />
                <UpdateUsernameForm />
                <UpdatePasswordForm />
                <NotificationsPreferences />
                <DataExport />
                {deleteAccount && <DeleteAccountForm />}
              </div>
            </div>
          </main>
        </>
      );
    }
  }
};

export default SettingsPage;
