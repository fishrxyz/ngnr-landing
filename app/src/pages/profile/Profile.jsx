import Footer from "../../components/profile/Footer";
import LoadingSpinner from "../../components/common/LoadingSpinner";

import "../../styles/profile/index.sass";
import useFetch from "../../hooks/useFetch";
import Error from "../../components/hoc/Error";
import ProfileDetails from "../../components/profile/ProfileDetails";
import { useParams } from "react-router-dom";

const Profile = () => {
  const { username } = useParams();
  const url = `${import.meta.env.VITE_API_BASE_URL}/n/${username}`;
  const { error, loading, data: profileInfo } = useFetch(url);

  if (error) {
    return <Error status={error.status} />;
  }

  if (loading || profileInfo == null) {
    return <LoadingSpinner message={"Fetching profile info ..."} />;
  } else {
    return (
      <>
        {error ? (
          <Error status={error.status} />
        ) : (
          <main className="profile">
            {profileInfo !== null && (
              <>
                <div className={`profile__container`}>
                  <ProfileDetails profileInfo={profileInfo} />
                </div>
                <Footer />
              </>
            )}
          </main>
        )}
      </>
    );
  }
};

export default Profile;
