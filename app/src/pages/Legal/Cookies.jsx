import { Helmet } from "react-helmet-async";

const CookiePolicy = () => {
  return (
    <>
      <Helmet>
        <title>Cookie Policy</title>
        <meta
          name="description"
          content="Cookie policy enforced at ngnr.club. The leading link in bio site for sotfware and devops engineers. Create a profile and get hired !"
        />
      </Helmet>
      <section className="legal-page">
        <div className="legal-page__subsection">
          <h2 className="legal-page__title">Cookie Policy</h2>
          <p className="legal-page__text">
            This is the Cookie Policy for ngnr.club, accessible from{" "}
            <a href="https://ngnr.club">https://ngnr.club</a>
          </p>
        </div>

        <div className="legal-page__subsection">
          <h3 className="legal-page__subtitle">1. What Are Cookies</h3>
          <p className="legal-page__text">
            As is common practice with almost all professional websites this
            site uses cookies, which are tiny files that are downloaded to your
            computer, to improve your experience. This page describes what
            information they gather, how we use it and why we sometimes need to
            store these cookies. We will also share how you can prevent these
            cookies from being stored however this may downgrade or 'break'
            certain elements of the sites functionality.
          </p>
        </div>

        <div className="legal-page__subsection">
          <h3 className="legal-page__subtitle">2. How We Use Cookies</h3>
          <p className="legal-page__text">
            We use cookies for a variety of reasons detailed below.
            Unfortunately in most cases there are no industry standard options
            for disabling cookies without completely disabling the functionality
            and features they add to this site. It is recommended that you leave
            on all cookies if you are not sure whether you need them or not in
            case they are used to provide a service that you use.
          </p>
        </div>

        <div className="legal-page__subsection">
          <h3 className="legal-page__subtitle">3. Disabling Cookies</h3>
          <p className="legal-page__text">
            You can prevent the setting of cookies by adjusting the settings on
            your browser (see your browser Help for how to do this). Be aware
            that disabling cookies will affect the functionality of this and
            many other websites that you visit. Disabling cookies will usually
            result in also disabling certain functionality and features of the
            this site. Therefore it is recommended that you do not disable
            cookies. This Cookies Policy was created with the help of the{" "}
            <a href="https://www.cookiepolicygenerator.com/cookie-policy-generator/">
              Cookies Policy Generator
            </a>
            .
          </p>
        </div>

        <div className="legal-page__subsection">
          <h3 className="legal-page__subtitle">4. The Cookies We Set</h3>
          <h4 className="legal-page__mini-title">a. Login related cookies</h4>
          <p className="legal-page__text">
            We use cookies when you are logged in so that we can remember this
            fact. This prevents you from having to log in every single time you
            visit a new page. These cookies are typically removed or cleared
            when you log out to ensure that you can only access restricted
            features and areas when logged in.
          </p>

          <h4 className="legal-page__mini-title">
            b. Site preferences cookies
          </h4>
          <p className="legal-page__text">
            In order to provide you with a great experience on this site we
            provide the functionality to set your preferences for how this site
            runs when you use it. In order to remember your preferences we need
            to set cookies so that this information can be called whenever you
            interact with a page is affected by your preferences.
          </p>
        </div>

        <div className="legal-page__subsection">
          <h3 className="legal-page__subtitle">5. More Information</h3>
          <p className="legal-page__text">
            Hopefully that has clarified things for you and as was previously
            mentioned if there is something that you aren't sure whether you
            need or not it's usually safer to leave cookies enabled in case it
            does interact with one of the features you use on our site.
          </p>
          <p className="legal-page__text">
            For more general information on cookies, please read{" "}
            <a href="https://www.cookiepolicygenerator.com/sample-cookies-policy/">
              the Cookies Policy article
            </a>
            .
          </p>
          <p className="legal-page__text">
            However if you are still looking for more information then you can
            contact us through one of our preferred contact methods:
          </p>
          <p className="legal-page__text">
            Email: <a href="mailto:karim@ngnr.club">karim@ngnr.club</a>
          </p>
        </div>
      </section>
    </>
  );
};

export default CookiePolicy;
