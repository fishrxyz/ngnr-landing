import axios from "redaxios";

export const getPublicProfileInfo = (username) => {
  return axios.get(`/api/n/${username}`);
};
