import {
  CognitoUserPool,
  CognitoUser,
  AuthenticationDetails,
  CognitoUserAttribute,
} from "amazon-cognito-identity-js";
import { cognitoConfig } from "../config/userpool";

const userPool = new CognitoUserPool({
  UserPoolId: cognitoConfig.UserPoolID,
  ClientId: cognitoConfig.ClientID,
});

export const register = (username, email, password) => {
  return new Promise((resolve, reject) => {
    const emailAttribute = new CognitoUserAttribute({
      Name: "email",
      Value: email,
    });
    const attributesList = [emailAttribute];
    userPool.signUp(username, password, attributesList, null, (err, result) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(result.user);
    });
  });
};

export const confirmRegister = (username, code) => {
  return new Promise((resolve, reject) => {
    const newCognitoUser = new CognitoUser({
      Username: username,
      Pool: userPool,
    });

    newCognitoUser.confirmRegistration(code, true, (err, result) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(result);
    });
  });
};

export const login = (username, password) => {
  return new Promise((resolve, reject) => {
    const authenticationDetails = new AuthenticationDetails({
      Username: username,
      Password: password,
    });

    const cognitoUser = new CognitoUser({
      Username: username,
      Pool: userPool,
    });

    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: (result) => resolve(result),
      onFailure: (error) => reject(error),
    });
  });
};

export const logout = () => {
  const currentUser = userPool.getCurrentUser();
  if (currentUser) {
    currentUser.signOut();
  }
};

export const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const currentUser = userPool.getCurrentUser();
    if (!currentUser) {
      reject(new Error("No user found"));
      return;
    }

    currentUser.getSession((err, session) => {
      if (err) {
        reject(err);
        return;
      }

      currentUser.getUserAttributes((err, attributes) => {
        if (err) {
          reject(err);
          return;
        }

        const userData = attributes.reduce((acc, attribute) => {
          acc[attribute.Name] = attribute.Value;
          return acc;
        }, {});

        resolve({ ...userData, username: currentUser.username, session });
      });
    });
  });
};

export const getSession = () => {
  const currentUser = userPool.getCurrentUser();
  return new Promise((resolve, reject) => {
    if (!currentUser) {
      reject(new Error("User not found"));
      return;
    }

    currentUser.getSession((err, session) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(session);
    });
  });
};

export const updateUserInfo = (username, email) => {
  return new Promise((resolve, reject) => {
    const currentUser = userPool.getCurrentUser();
    const newUsername = new CognitoUserAttribute({
      Name: "preferred_username",
      Value: username,
    });

    const newEmail = new CognitoUserAttribute({ Name: "email", Value: email });
    const attributesList = [newUsername, newEmail];

    currentUser.getSession((err, session) => {
      if (err) {
        reject(err);
        console.log(session);
        return;
      }
      currentUser.updateAttributes(attributesList, (err, result) => {
        if (err) {
          reject(err);
          return;
        }
        console.log(result);
        resolve(result);
      });
    });
  });
};

export const updatePassword = (oldpwd, newPwd) => {
  return new Promise((resolve, reject) => {
    const currentUser = userPool.getCurrentUser();

    currentUser.getSession((err, session) => {
      if (err) {
        reject(err);
        console.log(session);
        return;
      }

      currentUser.changePassword(oldpwd, newPwd, (err, result) => {
        if (err) {
          reject(err);
          return;
        }
        console.log(result);
        resolve(result);
      });
    });
  });
};

export const forgotPassword = (username) => {
  return new Promise((resolve, reject) => {
    const cognitoUser = new CognitoUser({
      Username: username,
      Pool: userPool,
    });

    cognitoUser.forgotPassword({
      onSuccess: () => {
        resolve();
      },
      onFailure: (err) => {
        reject(err);
      },
    });
  });
};

export const confirmResetPassword = (username, code, newPassword) => {
  return new Promise((resolve, reject) => {
    const cognitoUser = new CognitoUser({
      Username: username,
      Pool: userPool,
    });

    cognitoUser.confirmPassword(code, newPassword, {
      onSuccess: () => {
        resolve();
      },
      onFailure: (err) => {
        reject(err);
      },
    });
  });
};

export const resendConfirmationCode = (username) => {
  return new Promise((resolve, reject) => {
    const cognitoUser = new CognitoUser({
      Username: username,
      Pool: userPool,
    });

    cognitoUser.resendConfirmationCode((err, result) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(result);
    });
  });
};

export const deleteUserAccount = () => {
  return new Promise((resolve, reject) => {
    const currentUser = userPool.getCurrentUser();

    currentUser.getSession((err, session) => {
      if (err) {
        reject(err);
        console.log(session);
        return;
      }

      currentUser.deleteUser((err, result) => {
        if (err) {
          reject(err);
          return;
        }

        resolve(result);
      });
    });
  });
};
