import Avatar from "../assets/avatar.png";
import axios from "redaxios";

const buildProfileObject = (info) => {
  // NOTE: info is the api response object from axios
  const profileData = info.data;

  for (const key in profileData) {
    if (profileData[key] == "") {
      if (key == "avatar") {
        profileData[key] = Avatar;
      } else {
        profileData[key] = null;
      }
    }
  }
  return {
    info: {
      username: info.data.username,
      cv: info.data.cv,
      work: info.data.work_preferences,
      location: info.data.location,
      display_name: info.data.display_name,
      freelance: info.data.freelance,
      available: info.data.available,
      bio: info.data.bio,
      avatar: info.data.avatar,
      position: info.data.position,
    },
    social: {
      twitch: info.data.twitch,
      twitter: info.data.twitter,
      youtube: info.data.youtube,
      gitlab: info.data.gitlab,
      github: info.data.github,
      codepen: info.data.codepen,
      lobsters: info.data.lobsters,
      hn: info.data.hn,
    },
    contact: {
      email: info.data.email,
      blog: info.data.blog,
      nowPage: info.data.now_page,
      discord: info.data.discord,
      keybase: info.data.keybase,
      credly: info.data.credly,
    },
    projects: info.data.projects,
  };
};

export const downloadCV = async (e, url, filename) => {
  e.preventDefault();
  const res = await axios.get(url, {
    responseType: "blob",
    headers: {
      "Content-Type": "application/pdf",
    },
  });
  const blob = new Blob([res.data]);
  const downloadUrl = window.URL.createObjectURL(blob);

  // Create URL
  const link = document.createElement("a");
  link.href = downloadUrl;
  link.setAttribute("download", filename);

  // Append link to body
  document.body.appendChild(link);

  // Start download
  link.click();

  // Clean up and remove the link
  link.parentNode.removeChild(link);
};

export { buildProfileObject };
