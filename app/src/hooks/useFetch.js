import axios from "redaxios";
import { useEffect, useState } from "react";
import { PRIVATE_ENDPOINTS } from "../config/constants";

const useFetch = (url, token) => {
  const [data, setData] = useState(null);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setError(false); // reset error
    setLoading(true);
    const fetchData = async () => {
      let headers = {};
      if (PRIVATE_ENDPOINTS.includes(url)) {
        headers = {
          auth: token,
          "Access-Control-Allow-Origin": "*",
        };
      }
      try {
        const profileInfo = await axios.get(url, { headers });
        setData(profileInfo.data);
        setLoading(false);
      } catch (err) {
        if (err.data) {
          setError({
            status: err.status,
            message: err.data.message,
          });
        } else {
          setError({
            status: 500,
            message: "An error occured",
          });
        }
        setLoading(false);
      }
    };
    fetchData();
  }, [url, token]);

  return { data, error, loading };
};

export default useFetch;
