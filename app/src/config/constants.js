export const CONTACT_EMAIL = "ngnr.club@gmail.com";

export const PRIVATE_PROFILE_ENDPOINT = `${
  import.meta.env.VITE_API_BASE_URL
}/profile`;
export const AVATAR_PRESIGNED_URL_ENDPOINT = "/profile/avatar";
export const CV_PRESIGNED_URL_ENDPOINT = "/profile/cv";
export const EXPORT_DATA_URL_ENDPOINT = "/profile/data";
export const BASE_API_URL = import.meta.env.VITE_API_BASE_URL;
export const CV_MAX_FILE_SIZE = 3145728;

export const PRIVATE_ENDPOINTS = [
  PRIVATE_PROFILE_ENDPOINT,
  AVATAR_PRESIGNED_URL_ENDPOINT,
  CV_PRESIGNED_URL_ENDPOINT,
  EXPORT_DATA_URL_ENDPOINT,
];
