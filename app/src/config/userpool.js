export const cognitoConfig = {
  UserPoolID: import.meta.env.VITE_USER_POOL_ID,
  ClientID: import.meta.env.VITE_USER_POOL_CLIENT_ID,
};
