import PropTypes from "prop-types";
import { createContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import * as auth from "../helpers/auth";

const AuthContext = createContext();

const AuthProvider = ({ children }) => {
  const navigate = useNavigate();
  const [user, setUser] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const getCurrentUser = async () => {
    try {
      const user = await auth.getCurrentUser();
      setUser(user);
    } catch (error) {
      console.error(error.message);
      setUser(null);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getCurrentUser();
  }, []);

  const signIn = async (username, password) => {
    await auth.login(username, password);
    await getCurrentUser();
  };

  const signUp = async (username, email, password) => {
    await auth.register(username, email, password);
  };

  const confirmAccount = async (username, code) => {
    await auth.confirmRegister(username, code);
  };

  const signOut = async () => {
    await auth.logout();
    setUser(null);
    navigate("/auth/login");
  };

  const authValues = {
    user,
    isLoading,
    signIn,
    signOut,
    signUp,
    confirmAccount,
  };

  return (
    <AuthContext.Provider value={authValues}>{children}</AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.any,
};

export { AuthContext, AuthProvider };
