import PropTypes from "prop-types";
import axios from "redaxios";
import { createContext, useEffect, useState } from "react";
import {
  AVATAR_PRESIGNED_URL_ENDPOINT,
  BASE_API_URL,
  CV_PRESIGNED_URL_ENDPOINT,
  EXPORT_DATA_URL_ENDPOINT,
  PRIVATE_PROFILE_ENDPOINT,
} from "../config/constants";
import useFetch from "../hooks/useFetch";
import { useAuth } from "../hooks/useAuth";
import { updatePassword, updateUserInfo } from "../helpers/auth";

const ProfileContext = createContext();

const ProfileProvider = ({ children }) => {
  const [visibleForm, setVisibleForm] = useState(1);
  const [deleteMode, setDeleteMode] = useState(true);
  const [deleteAccount, setDeleteAccount] = useState(false);
  const [visibleSettingsForm, setVisibleSettingsForm] = useState(1);
  const [submitSuccess, setSubmitSuccess] = useState(false);
  const [submitError, setSubmitError] = useState(false);
  const [submitLoading, setSubmitLoading] = useState(false);
  const [profile, setProfile] = useState(null);
  const [modal, setShowModal] = useState(false);
  const auth = useAuth();
  const token = auth.user.session.accessToken.jwtToken;
  const { error, data: profileInfo } = useFetch(
    PRIVATE_PROFILE_ENDPOINT,
    token,
  );
  const [profileLoading, setProfileLoading] = useState(true);

  useEffect(() => {
    if (profileInfo != null) {
      if (profileInfo.data.projects === null) {
        profileInfo.data.projects = [];
      }
      setProfile(profileInfo.data);
      setProfileLoading(false);
    }
    if (error) {
      setProfileLoading(false);
    }
  }, [error, profileInfo]);

  const toggleForm = (id) => {
    setVisibleForm(id);
  };

  const toggleDeleteAccount = () => {
    setDeleteAccount(!deleteAccount);
  };

  const toggleSettingsForm = (id) => {
    setVisibleSettingsForm(id);
  };

  const setAddProjectMode = () => {
    setShowModal({ type: "add" });
  };

  const setEditProjectMode = (project) => {
    setShowModal({ type: "edit", project: project });
  };

  const closeModal = () => setShowModal(false);

  const uploadFileToS3 = async (url, file, headers) => {
    try {
      await fetch(url, {
        method: "PUT",
        body: file,
        headers: headers,
      });
      return url.split("?")[0];
    } catch (error) {
      console.error(error);
      setSubmitError({ msg: "Image upload failed." });
    }
  };

  const updateAvatarURL = async (url) => {
    setSubmitError(false);
    setSubmitSuccess(false);
    const headers = { auth: token };
    const payload = { avatar: url };
    try {
      const res = await axios.put(
        `${BASE_API_URL}${AVATAR_PRESIGNED_URL_ENDPOINT}`,
        payload,
        {
          headers,
        },
      );
      setProfile(res.data.data);
      setSubmitSuccess(true);
    } catch (error) {
      let msg;
      if (error.response) {
        msg = error.response.data.message;
        console.error(error.response.data.message);
      } else {
        msg = "An error occured";
        console.log(error);
      }
      setSubmitError({ msg });
    }
  };

  const updateCvURL = async (url) => {
    setSubmitError(false);
    setSubmitSuccess(false);
    const headers = { auth: token };
    const payload = { url };
    try {
      const res = await axios.put(
        `${BASE_API_URL}${CV_PRESIGNED_URL_ENDPOINT}`,
        payload,
        {
          headers,
        },
      );
      setProfile(res.data.data);
      setSubmitSuccess(true);
    } catch (error) {
      let msg;
      if (error.response) {
        msg = error.response.data.message;
        console.error(error.response.data.message);
      } else {
        msg = "An error occured";
        console.log(error);
      }
      setSubmitError({ msg });
    }
  };

  const requestPresignedURL = async (endpoint) => {
    const headers = { auth: token };
    const baseUrl = import.meta.env.VITE_API_BASE_URL;
    try {
      const res = await axios.get(`${baseUrl}${endpoint}`, { headers });
      return res.data.data.url;
    } catch (err) {
      let msg;
      if (error.response) {
        msg = error.response.data.message;
        console.error(error.response.data.message);
      } else {
        msg = "An error occured";
      }
      setSubmitError({ msg });
    }
  };

  // TODO: abstract the common logic into a separate function
  const setNewPassword = async (data) => {
    setSubmitLoading(true);
    setSubmitError(false);
    setSubmitSuccess(false);
    try {
      const { old_password, new_password } = data;
      await updatePassword(old_password, new_password);
      setSubmitSuccess(true);
    } catch (error) {
      const msg = "[ERROR] - " + error.message;
      console.error(msg);
      setSubmitError({ msg: error.message });
    }
    setSubmitLoading(false);
  };

  const updateGeneralSettings = async (data) => {
    setSubmitLoading(true);
    setSubmitError(false);
    setSubmitSuccess(false);
    try {
      const { username, email } = data;
      await updateUserInfo(username, email);
      setSubmitSuccess(true);
    } catch (error) {
      const msg = "[ERROR] - " + error.message;
      console.error(msg);
      setSubmitError({ msg: error.message });
    }
    setSubmitLoading(false);
  };

  const updateProfile = async (data, reset) => {
    setSubmitLoading(true);
    setSubmitError(false);
    setSubmitSuccess(false);

    const headers = { auth: token };
    try {
      const payload = { ...profile, ...data };
      const res = await axios.put(PRIVATE_PROFILE_ENDPOINT, payload, {
        headers,
      });
      if (reset) {
        reset(res.data.data);
      }
      const updatedProfile = { ...res.data.data, avatar: profile.avatar };
      setProfile(updatedProfile);
      setSubmitSuccess(true);
    } catch (error) {
      let msg;
      if (error.response) {
        msg = error.response.data.message;
        console.error(error.response.data.message);
      } else {
        msg = "An error occured";
      }
      setSubmitError({ msg });
    }
    setSubmitLoading(false);
  };

  const requestDataExport = async () => {
    setSubmitLoading(true);
    setSubmitError(false);
    setSubmitSuccess(false);

    const headers = { auth: token };
    try {
      const res = await axios.get(
        `${BASE_API_URL}${EXPORT_DATA_URL_ENDPOINT}`,
        { headers },
      );
      console.log(res);
      setSubmitSuccess(true);
    } catch (error) {
      let msg;
      if (error.response) {
        msg = error.response.data.message;
        console.error(error.response.data.message);
      } else {
        msg = "An error occured";
      }
      setSubmitError({ msg });
    }
    setSubmitLoading(false);
  };

  const values = {
    visibleForm,
    setVisibleForm,
    toggleForm,
    submitSuccess,
    setSubmitSuccess,
    submitError,
    setSubmitError,
    submitLoading,
    setSubmitLoading,
    error,
    profileInfo,
    modal,
    profileLoading,
    profile,
    updateProfile,
    closeModal,
    setAddProjectMode,
    setEditProjectMode,
    requestPresignedURL,
    updateAvatarURL,
    uploadFileToS3,
    toggleSettingsForm,
    visibleSettingsForm,
    deleteAccount,
    toggleDeleteAccount,
    updateGeneralSettings,
    setNewPassword,
    deleteMode,
    setDeleteMode,
    username: auth.user.username,
    requestDataExport,
    updateCvURL,
  };

  return (
    <ProfileContext.Provider value={values}>{children}</ProfileContext.Provider>
  );
};

ProfileProvider.propTypes = {
  children: PropTypes.any,
};

export { ProfileContext, ProfileProvider };
