import { RefreshDouble } from "iconoir-react";
import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import Notification from "../common/Notification";
import { useAuth } from "../../hooks/useAuth.js";
import { resendConfirmationCode } from "../../helpers/auth";
import OTPInput from "react-otp-input";

const ConfirmRegisterForm = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const [username, setUsername] = useState(location.state?.username || "");
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [resentSuccess, setResentSuccess] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [otp, setOtp] = useState("");

  const auth = useAuth();

  const resendCode = async () => {
    setError("");
    try {
      await resendConfirmationCode(username);
      setResentSuccess(true);
    } catch (error) {
      let msg = "Error sending code";
      console.log(error);
      setError({ msg });
    }
  };

  const handleSubmit = async (e) => {
    setIsSubmitting(true);
    e.preventDefault();
    setError("");

    try {
      await auth.confirmAccount(username, otp);
      setSuccess(true);
    } catch (error) {
      let msg = "Error validating code";
      if (error.name == "CodeMismatchException") {
        msg = "Invalid Verification Code";
      } else if (error.name == "ExpiredCodeException") {
        msg = "This code has expired";
      }
      console.log(error);
      setError({ msg });
    } finally {
      setIsSubmitting(false);
    }
  };

  useEffect(() => {
    if (success) {
      navigate("/auth/login", {
        state: { message: "You've successfully confirnmed your email !" },
      });
    }
  }, [navigate, success]);

  return (
    <>
      {error && <Notification fixed error message={error.msg} />}
      {resentSuccess && (
        <Notification fixed message={"Check your email for the new code !"} />
      )}
      <form onSubmit={handleSubmit} className="auth__form register-form">
        <div className="form__field">
          <label htmlFor="username" className="field__label">
            <span className="field__label__text">Username</span>
            <input
              type="text"
              name="username"
              id="username"
              disabled={location.state?.username}
              className="field__input"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </label>
        </div>
        <div className="form__field field-otp">
          <label htmlFor="code" className="field__label">
            <span className="field__label__text">Confirmation Code</span>
            <OTPInput
              value={otp}
              onChange={setOtp}
              containerStyle={{ justifyContent: "space-around" }}
              inputStyle={{ flexBasis: "13%" }}
              numInputs={6}
              renderSeparator={<span>-</span>}
              shouldAutoFocus={true}
              renderInput={(props) => (
                <input {...props} type="text" className="field__input" />
              )}
            />
          </label>
        </div>
        <button
          className={`auth__form__submit ${isSubmitting ? "submitting" : ""}`}
          type="submit"
        >
          <p className="auth__form__submit__text">
            {isSubmitting ? (
              <>
                <span>Checking code ...</span>
                <i>
                  <RefreshDouble width="13" height="13" strokeWidth="3" />
                </i>
              </>
            ) : (
              <>
                <span>Confirm my account</span>
              </>
            )}
          </p>
        </button>
      </form>

      {location.state?.username && (
        <div className="auth__form__resend-link">
          <p>Didn't receive the code ?</p>
          <p>
            Check your spam folder or{" "}
            <span onClick={resendCode} className="auth__form__link">
              send again
            </span>{" "}
          </p>
        </div>
      )}
    </>
  );
};

export default ConfirmRegisterForm;
