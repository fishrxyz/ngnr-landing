import { RefreshDouble } from "iconoir-react";
import { useEffect, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import Notification from "../common/Notification";
import { useAuth } from "../../hooks/useAuth.js";

const RegisterForm = () => {
  const location = useLocation();
  window.history.replaceState({}, document.title);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState(location.state?.username || "");
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);
  const navigate = useNavigate();
  const auth = useAuth();

  const handleSubmit = async (e) => {
    setIsSubmitting(true);
    e.preventDefault();
    setError("");
    try {
      await auth.signUp(username, email, password);
      setSuccess(true);
    } catch (err) {
      let msg = "An error occured";
      if (
        err.name == "InvalidPasswordException" ||
        err.name === "UsernameExistsException"
      ) {
        msg = err.message;
      }

      console.error(err.name);
      console.error(err.message);
      setError({ name: err.name, msg });
    } finally {
      setIsSubmitting(false);
    }
  };

  useEffect(() => {
    if (success) {
      navigate("/auth/confirm", { replace: true, state: { email, username } });
    }
  }, [success, username, email, navigate]);

  return (
    <>
      {error && error.name != "InvalidPasswordException" && (
        <Notification fixed error message={error.msg} />
      )}
      <form onSubmit={handleSubmit} className="auth__form register-form">
        <div className="form__field">
          <label htmlFor="username" className="field__label">
            <span className="field__label__text">Username</span>
            <input
              type="text"
              name="username"
              id="username"
              className="field__input"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="email" className="field__label">
            <span className="field__label__text">Email Address</span>
            <input
              type="email"
              name="email"
              id="email"
              className="field__input"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </label>
        </div>
        <div className="form__field">
          <label
            htmlFor="password"
            className={`field__label ${
              error && error.name == "InvalidPasswordException"
                ? "validation-input-error"
                : ""
            }`}
          >
            <span className="field__label__text">Password</span>
            <input
              type="password"
              name="password"
              id="password"
              className="field__input"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <div className="form__field__message">
              {error && error.name == "InvalidPasswordException" ? (
                <p className="validation-error">{error.msg}</p>
              ) : (
                <p>
                  Password must contain at least 8 characters, including an
                  uppercase letter, a lowercase letter, and a number
                </p>
              )}
            </div>
          </label>
        </div>

        <button
          className={`auth__form__submit ${isSubmitting ? "submitting" : ""}`}
          type="submit"
        >
          <p className="auth__form__submit__text">
            {isSubmitting ? (
              <>
                <span>Registering account ... </span>
                <i>
                  <RefreshDouble width="13" height="13" strokeWidth="3" />
                </i>
              </>
            ) : (
              <>
                <span>Register with email</span>
              </>
            )}
          </p>
        </button>
      </form>
      <footer className="auth__form__footer">
        <p className="auth__form__cta">
          Already have an account ? <Link to="/auth/login">Login</Link>
        </p>
        <p className="auth__form__tnc">
          By creating an account you agree to our{" "}
          <Link to="/terms">
            <strong>Terms & Conditions</strong> and{" "}
            <strong>Privacy Statement</strong>.
          </Link>
        </p>
      </footer>
    </>
  );
};

export default RegisterForm;
