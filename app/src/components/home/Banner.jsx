const Banner = () => {
  return (
    <section className="home__banner">
      <div className="home__banner__jp">
        <p>エンジニアドットクラブ</p>
      </div>
    </section>
  );
};

export default Banner;
