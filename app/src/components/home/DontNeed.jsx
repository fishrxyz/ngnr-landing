const DontNeed = () => {
  return (
    <section className="home__dont-need">
      <ul className="dont-need__list">
        <li className="dont-need__list__item">You don&apos;t need LinkedIn</li>
        <li className="dont-need__list__item">You don&apos;t need a UI</li>
        <li className="dont-need__list__item">You don&apos;t need a QR Code</li>
        <li className="dont-need__list__item">
          You don&apos;t need a business card
        </li>
        <li className="dont-need__list__item">
          You don&apos;t need to be the product
        </li>
      </ul>
    </section>
  );
};

export default DontNeed;
