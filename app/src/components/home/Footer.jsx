import { Link } from "react-router-dom";
import { CONTACT_EMAIL } from "../../config/constants";

const Footer = () => {
  const links = [
    { name: "Contact", external: true, href: `mailto:${CONTACT_EMAIL}` },
    { name: "Twitter", external: true, href: "https://twitter.com/ngnrclub" },
    { name: "Twitch", external: true, href: "https://twitch.tv/ayofishr" },
    { name: "Discord", external: true, href: "https://discord.gg/zHnwbVHPUX" },
    {
      name: "Changelog",
      external: false,
      href: "/changelog",
    },
    { name: "FAQ", external: false, href: "/faq" },
    { name: "Log In", external: false, href: "/auth/login" },
    { name: "Sign Up", external: false, href: "/auth/register" },
    { name: "Terms", external: false, href: "/legal/terms" },
    { name: "Privacy", external: false, href: "/legal/privacy" },
  ];

  return (
    <footer className="home__footer">
      <div className="home__footer__section footer__about">
        <p>
          ngnr.club is built in public by{" "}
          <a href="https://0xfishr.xyz">@ayofishr</a>.
        </p>
        <p>We hope you are having a great day.</p>
      </div>
      <div className="home__footer__section footer__links">
        <ul className="footer__links__list">
          {links.map((link, idx) => (
            <li key={idx} className="footer__links__link">
              {link.external ? (
                <a href={link.href}>{link.name}</a>
              ) : (
                <Link to={link.href}>{link.name}</Link>
              )}
            </li>
          ))}
        </ul>
      </div>
    </footer>
  );
};

export default Footer;
