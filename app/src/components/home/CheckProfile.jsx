import { useState } from "react";
import { useForm } from "react-hook-form";
import axios from "redaxios";
import { useNavigate } from "react-router-dom";
import { BrightStar } from "iconoir-react";

const CheckProfile = () => {
  const { register, handleSubmit, reset } = useForm();

  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const navigate = useNavigate();

  const createAccount = (username) => {
    navigate("/auth/register", { state: { username } });
  };

  const checkUsername = async (data) => {
    setError(false);
    setSuccess(false);
    setIsSubmitting(true);
    const { username } = data;

    try {
      await axios.get(`${import.meta.env.VITE_API_BASE_URL}/n/${username}`);
      setError({
        msg: `Sorry, @${username} is already taken.`,
      });
      setIsSubmitting(false);
    } catch (error) {
      if (error.status === 404) {
        setSuccess({ msg: `Yes, @${username} is available ! `, username });
      } else {
        setError({
          msg: "Hmmm, an error occured server side.",
        });
      }
      setIsSubmitting(false);
    }

    reset();
  };

  return (
    <section className="home__check-profile">
      <div className="home__check-profile__container">
        <h3 className="check__profile__title">
          It starts with <span>your name</span>.
        </h3>
        <form onSubmit={handleSubmit(checkUsername)}>
          <input
            type="text"
            placeholder="type your desired username here"
            {...register("username", { required: true })}
          />
        </form>
        <div className="check__profile__message">
          <p>
            {isSubmitting && <>Checking availability...</>}
            {success && (
              <>
                <span>
                  <BrightStar width={27} height={27} strokeWidth={2} />{" "}
                  {success.msg}
                </span>
                <button
                  className="check__profile__button"
                  onClick={() => createAccount(success.username)}
                >
                  Secure it now
                </button>
              </>
            )}
            {error && <>{error.msg}</>}
            {!success && !error && !isSubmitting && (
              <>(* We&apos;ll tell you if it&apos;s available.)</>
            )}
          </p>
        </div>
      </div>
    </section>
  );
};

export default CheckProfile;
