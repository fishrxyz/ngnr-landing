const HowItWorks = () => {
  return (
    <section className="home__how-it-works">
      <ul className="home__how-it-works__list">
        <li className="how-it-works__item">Create an account.</li>
        <li className="how-it-works__item">Add your info ...</li>
        <li className="how-it-works__item">... And your links.</li>
        <li className="how-it-works__item">Live life.</li>
      </ul>
    </section>
  );
};

export default HowItWorks;
