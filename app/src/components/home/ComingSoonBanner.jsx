const ComingSoonBanner = () => {
  return (
    <div className="home__coming-soon-banner">
      <div className="coming-soon__container">
        <p>
          <span>Good News !</span> <span>v3 has finally launched !</span>
        </p>
      </div>
    </div>
  );
};

export default ComingSoonBanner;
