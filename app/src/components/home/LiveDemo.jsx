import { Link } from "react-router-dom";

const LiveDemo = () => {
  return (
    <article className="home__live-demo">
      <p className="home__live-demo__text">
        There is also a{" "}
        <span>
          <i></i>live
        </span>{" "}
        demo for you to <Link to="/n/ayofishr">check out</Link>.
      </p>
    </article>
  );
};

export default LiveDemo;
