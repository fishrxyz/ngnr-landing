const Blurb = () => {
  return (
    <article className="home__blurb">
      <p className="home__blurb__copy">
        We are a link-in-bio service aimed at engineers, just like you. Create
        your very own internet home page and share it with prospects/recruiters
        on social media to find your next client/gig !
      </p>
    </article>
  );
};

export default Blurb;
