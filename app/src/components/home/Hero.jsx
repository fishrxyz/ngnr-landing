import LogoAlt from "../../assets/LogoAlt";

const Hero = () => {
  return (
    <div className="home__hero">
      <LogoAlt />
    </div>
  );
};

export default Hero;
