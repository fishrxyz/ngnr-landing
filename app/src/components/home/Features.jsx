const Features = () => {
  return (
    <section className="home__features">
      <div className="feature feature--one">
        <h4 className="feature__title">Your privacy matters to us</h4>
        <p className="feature__description">
          We don&apos;t mess about with privacy. Expect no cookies, no data
          logging, no selling your info to third parties.
        </p>
      </div>
      <div className="feature feature--two">
        <h4 className="feature__title">Built for you by real people</h4>
        <p className="feature__description">
          Enjoy continious improvements to the site, immediate support from a
          real person and a fun community on Discord.
        </p>
      </div>

      <div className="feature feature--three">
        <h4 className="feature__title">Showcase your best work</h4>
        <p className="feature__description">
          Connect your GitHub and/or GitLab account and select your best repos
          to put on your front page.
        </p>
      </div>

      <div className="feature feature--four">
        <h4 className="feature__title">An exclusive Job Board</h4>
        <p className="feature__description">
          We&apos;re working hard to connect our amazing community with amazing
          companies around the world.
        </p>
      </div>
    </section>
  );
};

export default Features;
