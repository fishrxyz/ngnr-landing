import HackingGif from "../../assets/gifs/waifuhacking.gif";

const Gif = () => {
  return (
    <div className="home__gif">
      <img src={HackingGif} alt="mamahacker" />
    </div>
  );
};

export default Gif;
