import PropTypes from "prop-types";
import { IconoirProvider, InfoEmpty, WarningCircle } from "iconoir-react";
import { useEffect, useState } from "react";

const Notification = ({ fixed, error, mini, message }) => {
  const [shouldRender, setShouldRender] = useState(true);

  useEffect(() => {
    if (!fixed) {
      setTimeout(() => {
        setShouldRender(false);
      }, 3000);
    }
  }, [fixed]);

  const iconsProps = {
    width: "20",
    height: "20",
    strokeWidth: 2,
  };
  return (
    <>
      {shouldRender && (
        <IconoirProvider iconProps={iconsProps}>
          <div
            className={`ngnr__notification ${error ? "error" : ""} ${
              mini && "mini"
            }`}
          >
            <i className="ngnr__notification__icon">
              {error ? <WarningCircle /> : <InfoEmpty />}
            </i>
            <span className="ngnr__notification__text">{message}</span>
          </div>
        </IconoirProvider>
      )}
    </>
  );
};

Notification.propTypes = {
  error: PropTypes.bool,
  mini: PropTypes.bool,
  message: PropTypes.string,
  fixed: PropTypes.bool,
};

export default Notification;
