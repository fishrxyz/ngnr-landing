import { RefreshDouble } from "iconoir-react";
import LogoAlt from "../../assets/LogoAlt";

const AuthLoading = () => {
  return (
    <section className="ngnr__protected__loading">
      <div className="protected__loading__container">
        <div className="logo-wrapper">
          <LogoAlt />
        </div>
        <div className="loading-spinner">
          <RefreshDouble strokeWidth={3} />
        </div>
      </div>
    </section>
  );
};

export default AuthLoading;
