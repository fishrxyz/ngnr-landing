import {
  IconoirProvider,
  Lifebelt,
  LogOut,
  MoreHoriz,
  Settings,
  Www,
} from "iconoir-react";
import { useState } from "react";
import { Link, NavLink } from "react-router-dom";
import LogoAlt from "../../assets/LogoAlt";
import { useAuth } from "../../hooks/useAuth.js";

const Menu = () => {
  const auth = useAuth();

  return (
    <IconoirProvider
      iconProps={{
        width: 20,
        height: 20,
        strokeWidth: 2.5,
      }}
    >
      <ul className="ngnr__nav__menu">
        <li className="menu__item">
          <NavLink to="/settings" className="menu__item__link">
            <span>Settings</span>
            <i>
              <Settings />
            </i>
          </NavLink>
        </li>
        <li className="menu__item">
          <a href="https://discord.gg/6EDTZHkd" className="menu__item__link">
            <span>Get Help</span>
            <i>
              <Lifebelt />
            </i>
          </a>
        </li>
        <li className="menu__item">
          <button onClick={auth.signOut} className="menu__item__link">
            <span>Sign Out</span>
            <i>
              <LogOut />
            </i>
          </button>
        </li>
      </ul>
    </IconoirProvider>
  );
};

const Nav = () => {
  const [showMenu, setShowMenu] = useState(false);
  const auth = useAuth();
  return (
    <nav className="ngnr__nav">
      <div className="ngnr__nav__container">
        <Link className="ngnr__nav__logo" to="/profile">
          <LogoAlt />
        </Link>

        <div className="ngnr__nav__middle">
          <Link
            className="ngnr__nav__profile-link"
            to={`/n/${auth.user.preferred_username}`}
          >
            <span>ngnr.club/n/@{auth.user.preferred_username}</span>
            <Www width="22" height="22" />
          </Link>
        </div>

        <button
          onClick={() => setShowMenu(!showMenu)}
          className={`ngnr__nav__hamburger ${showMenu && "show-menu"}`}
        >
          <MoreHoriz width="25" height="25" strokeWidth="2" />
        </button>
        {showMenu && (
          <div
            onClick={() => setShowMenu(false)}
            className="ngnr__menu__overlay"
          >
            <Menu />
          </div>
        )}
      </div>
    </nav>
  );
};

export default Nav;
