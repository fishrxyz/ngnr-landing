import PropTypes from "prop-types";
import {
  CheckCircle,
  DeleteCircle,
  InfoEmpty,
  WarningCircle,
} from "iconoir-react";

const FormPill = ({ type, message }) => {
  let Icon;
  if (type == "success") {
    Icon = CheckCircle;
  } else if (type == "error") {
    Icon = DeleteCircle;
  } else if (type == "warning") {
    Icon = WarningCircle;
  } else {
    Icon = InfoEmpty;
  }

  return (
    <div className={`form__pill ${type}`}>
      <Icon width={15} height={15} strokeWidth={2.2} />
      <span>{message}</span>
    </div>
  );
};

FormPill.propTypes = {
  type: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
};

export default FormPill;
