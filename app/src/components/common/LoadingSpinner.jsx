import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import LoadingOne from "../../assets/gifs/loading-1.gif";
import LoadingTwo from "../../assets/gifs/loading-2.gif";
import LoadingThree from "../../assets/gifs/loading-3.gif";
import LoadingFive from "../../assets/gifs/loading-5.gif";
import LoadingSix from "../../assets/gifs/loading-6.gif";
import LoadingSeven from "../../assets/gifs/loading-7.gif";
import LoadingEight from "../../assets/gifs/loading-8.gif";
import LoadingNine from "../../assets/gifs/loading-9.gif";

const LoadingSpinner = ({ message }) => {
  const [gif, setGif] = useState("");

  useEffect(() => {
    const gifs = [
      LoadingOne,
      LoadingTwo,
      LoadingThree,
      LoadingFive,
      LoadingSix,
      LoadingSeven,
      LoadingEight,
      LoadingNine,
    ];

    const randomGif = gifs[Math.floor(Math.random() * gifs.length)];
    setGif(randomGif);
  }, []);

  return (
    <div className="ngnr__loading__spinner">
      <div className="ngnr__loading__spinner__container">
        <figure className="ngnr__loading__image">
          <img src={gif} alt="loading spinner" />
        </figure>
        <article className="ngnr__loading__text">
          <h3 className="ngnr__loading__title">Loading...</h3>
          <p className="ngnr__loading__message">{message}</p>
        </article>
      </div>
    </div>
  );
};

LoadingSpinner.propTypes = {
  message: PropTypes.string,
};

export default LoadingSpinner;
