import FaqGif from "../../assets/gifs/faq.gif";

const Header = () => {
  return (
    <header className="faq__header">
      <h2 className="faq__title">Frequently Asked Questions</h2>
      <p className="faq__subtitle">
        Everything you need to know to start using ngnr.club.
      </p>
      <figure className="faq__gif">
        <img src={FaqGif} alt="Frequently asked questions" />
      </figure>
    </header>
  );
};

export default Header;
