import questions from "../../data/faq.json";

const Questions = () => {
  return (
    <div className="faq__questions">
      {questions.map((question, idx) => (
        <div key={idx} className="faq__question">
          <h3 className="faq__question__title">{question.question}</h3>
          <p className="faq__question__answer">{question.answer}</p>
        </div>
      ))}
    </div>
  );
};

export default Questions;
