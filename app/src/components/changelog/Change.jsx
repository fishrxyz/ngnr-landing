import {
  BrightStar,
  Calendar,
  CodeBracketsSquare,
  IconoirProvider,
} from "iconoir-react";
import PropTypes from "prop-types";

const Change = ({ change }) => {
  return (
    <IconoirProvider
      iconProps={{
        width: 18,
        height: 18,
        strokeWidth: 2.2,
      }}
    >
      <div className="changelog__change">
        <div className="changelog__metadata">
          <span className="changelog__date">
            <Calendar />
            {change.date}
          </span>
          <span className="changelog__type">
            <CodeBracketsSquare />
            {change.type}
          </span>
          <span className="changelog__version">
            <BrightStar />v{change.version}
          </span>
        </div>
        <h2 className="changelog__title">{change.title}</h2>
        <p className="changelog__description">{change.description}</p>
        <ul className="changelog__details">
          {change.details.map((detail, idx) => (
            <li className="changelog__item" key={idx}>
              <span className="changelog__item__star">*</span>
              <span className="changelog__item__content">{detail}</span>
            </li>
          ))}
        </ul>
      </div>
    </IconoirProvider>
  );
};

Change.propTypes = {
  change: PropTypes.object,
};

export default Change;
