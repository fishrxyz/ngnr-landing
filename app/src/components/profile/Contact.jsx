import PropTypes from "prop-types";
import {
  Discord,
  Heart,
  IconoirProvider,
  JournalPage,
  Lock,
  Mail,
  Medal1St,
} from "iconoir-react";

const Contact = ({ links }) => {
  const IconProps = {
    strokeWidth: 2,
    width: "24",
    height: "24",
  };
  return (
    <section className="profile__section profile__contact">
      <header className="profile__header">
        <h2 className="contact__title">Contact and stuff</h2>
      </header>

      <ul className="contact__links">
        <IconoirProvider iconProps={IconProps}>
          {links.email && (
            <li className="contact__link__item">
              <i className="contact__link__icon">
                <Mail />
              </i>
              <a className="contact__link__href" href={`mailto:${links.email}`}>
                {links.email}
              </a>
            </li>
          )}
          {links.blog && (
            <li className="contact__link__item">
              <i className="contact__link__icon">
                <JournalPage />
              </i>
              <a className="contact__link__href" href={links.blog}>
                /notes
              </a>
            </li>
          )}

          {links.nowPage && (
            <li className="contact__link__item">
              <i className="contact__link__icon">
                <Heart />
              </i>
              <a className="contact__link__href" href={links.nowPage}>
                /now
              </a>
            </li>
          )}
          {links.discord && (
            <li className="contact__link__item">
              <i className="contact__link__icon">
                <Discord />
              </i>
              <a className="contact__link__href" href="#">
                {links.discord}
              </a>
            </li>
          )}
          {links.keybase && (
            <li className="contact__link__item">
              <i className="contact__link__icon">
                <Lock />
              </i>
              <a className="contact__link__href" href={links.keybase}>
                keybase.io
              </a>
            </li>
          )}
          {links.credly && (
            <li className="contact__link__item">
              <i className="contact__link__icon">
                <Medal1St />
              </i>
              <a className="contact__link__href" href={links.credly}>
                certification badges
              </a>
            </li>
          )}
        </IconoirProvider>
      </ul>
    </section>
  );
};

Contact.propTypes = {
  links: PropTypes.object,
};

export default Contact;
