import NotFoundGif from "../../assets/profile-not-found.gif";

const DoesNotExist = () => {
  return (
    <section className="profile__not-found">
      <article className="profile__not-found__text">
        <h2 className="profile__not-found__title">Profile not found.</h2>
        <p className="profile__not-found__copy">
          We looked everywhere, but it seems that this profile does not exist or
          has been removed.
        </p>
        <p className="profile__not-found__copy">
          Sorry about that! (Yes, did try rebooting.)
        </p>
      </article>
      <figure className="profile__not-found__gif">
        <img src={NotFoundGif} alt="not found" />
      </figure>
    </section>
  );
};

export default DoesNotExist;
