import PropTypes from "prop-types";

const Projects = ({ projects }) => {
  return (
    <section className="profile__section profile__projects">
      <header className="profile__header">
        <h2 className="projects__title">Recent Projects</h2>
      </header>
      <ul className="projects__list">
        {projects &&
          projects.map((project, idx) => (
            <li key={idx} className="project__item">
              <div className="project__details">
                <h4 className="project__name">
                  <a href={project.url}>{project.name}</a>
                </h4>
                <p className="project__about">{project.description}</p>
              </div>
            </li>
          ))}
      </ul>
    </section>
  );
};

Projects.propTypes = {
  projects: PropTypes.array,
};

export default Projects;
