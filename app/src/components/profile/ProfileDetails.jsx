import PropTypes from "prop-types";
import Projects from "./Projects";
import Info from "./Info";
import Social from "./Social";
import Contact from "./Contact";
import SEO from "../hoc/SEO";
import { buildProfileObject } from "../../helpers/profile";

const ProfileDetails = ({ profileInfo }) => {
  const profile = buildProfileObject(profileInfo);

  return (
    <>
      <SEO
        title={`@${profile.info.username}`}
        description={profile.info.bio}
        favicon={profile.info.avatar}
      />
      <Info info={profile.info} />
      <Projects projects={profile.projects} />
      <Social links={profile.social} />
      <Contact links={profile.contact} />
    </>
  );
};

ProfileDetails.propTypes = {
  profileInfo: PropTypes.object,
};

export default ProfileDetails;
