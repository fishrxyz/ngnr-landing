import { Link } from "react-router-dom";
import LogoAlt from "../../assets/LogoAlt";

const Footer = () => {
  return (
    <footer className="profile__footer">
      <div className="footer__container">
        <Link to="/">
          <LogoAlt />
        </Link>
      </div>
    </footer>
  );
};

export default Footer;
