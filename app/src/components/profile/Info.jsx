import PropTypes from "prop-types";
import {
  DownloadCircle,
  EuroSquare,
  Globe,
  LargeSuitcase,
  MoneySquare,
  PinAlt,
  Tools,
  VerifiedBadge,
} from "iconoir-react";
import { downloadCV } from "../../helpers/profile";

const Info = ({ info }) => {
  return (
    <section className="profile__info">
      <div className="container">
        {info.cv.url !== "" && (
          <button
            onClick={(e) =>
              downloadCV(e, info.cv.url, `${info.username}-cv.pdf`)
            }
            className="profile__info__download"
          >
            <DownloadCircle width={22} height={22} strokeWidth={2.2} />
            <span>Download my CV</span>
          </button>
        )}
        <figure
          className="profile__avatar"
          style={{ backgroundImage: `url(${info.avatar})` }}
        ></figure>
        <div className="profile__info__details">
          {info.available && (
            <div className="profile__available-badge">
              I&apos;m available for work !
            </div>
          )}
          <span className="profile__username">@{info.username}</span>
          <h3 className="profile__display-name">
            <span>{info.display_name}</span>
            <VerifiedBadge width="45" height="45" fill="#28ADE6" color="#fff" />
          </h3>
          <ul className="profile__misc">
            <li className="profile__misc__item">
              <PinAlt height="15" width="15" strokeWidth="2.5" />
              <span>{info.location}</span>
            </li>
            <li className="profile__misc__item">
              <Tools height="15" width="15" strokeWidth="2.5" />
              <span>{info.position}</span>
            </li>
          </ul>
          <article className="profile__bio">
            <p>{info.bio}</p>
          </article>
          <ul className="profile__work__info">
            {info.work.remote_ok && (
              <li className="profile__work__item">
                <Globe height="15" width="15" strokeWidth="2.5" />
                <span>Remote OK</span>
              </li>
            )}
            {info.work.eu_ok && (
              <li className="profile__work__item">
                <EuroSquare height="15" width="15" strokeWidth="2.5" />
                <span>EU eligible</span>
              </li>
            )}
            {info.work.us_ok && (
              <li className="profile__work__item">
                <MoneySquare height="15" width="15" strokeWidth="2.5" />
                <span>US eligible</span>
              </li>
            )}
            {info.work.freelance && (
              <li className="profile__work__item">
                <LargeSuitcase height="15" width="15" strokeWidth="2.5" />
                <span>Freelancer</span>
              </li>
            )}
          </ul>
        </div>
      </div>
    </section>
  );
};

Info.propTypes = {
  info: PropTypes.object,
};

export default Info;
