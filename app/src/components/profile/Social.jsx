import PropTypes from "prop-types";
import {
  Codepen,
  Computer,
  GitLabFull,
  IconoirProvider,
  Journal,
  Twitter,
  YouTube,
} from "iconoir-react";
import Twitch from "../../assets/icons/Twitch";

const Social = ({ links }) => {
  const IconProps = {
    color: "#FFFFFF",
    strokeWidth: 2,
    width: "24",
    height: "24",
  };

  return (
    <section className="profile__section profile__social">
      <header className="profile__header">
        <h2 className="social__title">Elsewhere on the web</h2>
      </header>
      <IconoirProvider iconProps={IconProps}>
        <ul className="social__links">
          {links.twitch && (
            <li className="social__link">
              <a href={links.twitch}>
                <i className="icon-twitch">
                  <Twitch />
                </i>
                <span>Watch me live on Twitch</span>
              </a>
            </li>
          )}
          {links.twitter && (
            <li className="social__link">
              <a href={links.twitter}>
                <i className="icon-twitter">
                  <Twitter />
                </i>
                <span>Follow me on Twitter</span>
              </a>
            </li>
          )}
          {links.gitlab && (
            <li className="social__link">
              <a href={links.gitlab}>
                <i className="icon-gitlab">
                  <GitLabFull />
                </i>
                <span>Have a look at my code</span>
              </a>
            </li>
          )}
          {links.youtube && (
            <li className="social__link">
              <a href={links.youtube}>
                <i className="icon-yt">
                  <YouTube />
                </i>
                <span>Subscribe to my channel</span>
              </a>
            </li>
          )}
          {links.codepen && (
            <li className="social__link">
              <a href={links.codepen}>
                <i className="icon-cp">
                  <Codepen />
                </i>
                <span>My UI experiments</span>
              </a>
            </li>
          )}
          {links.lobsters && (
            <li className="social__link">
              <a href={links.lobsters}>
                <i className="icon-lobsters">
                  <Computer />
                </i>
                <span>lobsters.rs</span>
              </a>
            </li>
          )}
          {links.hn && (
            <li className="social__link">
              <a href={links.hn}>
                <i className="icon-hn">
                  <Journal />
                </i>
                <span>Hacker News</span>
              </a>
            </li>
          )}
        </ul>
      </IconoirProvider>
    </section>
  );
};

Social.propTypes = {
  links: PropTypes.object,
};

export default Social;
