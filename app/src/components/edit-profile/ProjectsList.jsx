import {
  Calendar,
  EditPencil,
  GithubCircle,
  IconoirProvider,
  Link,
  Trash,
} from "iconoir-react";
import PropTypes from "prop-types";
import { useContext } from "react";
import { ProfileContext } from "../../context/ProfileContext";

const DeleteProjectModal = ({ projectIndex }) => {
  const { profile, updateProfile, setDeleteMode, submitLoading } =
    useContext(ProfileContext);

  const deleteProject = async () => {
    profile.projects.splice(projectIndex, 1);
    await updateProfile(profile);
    console.log(projectIndex);
    setDeleteMode(false);
  };

  return (
    <div className="project__item__delete">
      <div>
        <div className="project__item__delete__text">
          <h3>Delete Project ?</h3>
          <p>Note: This action is irreversible.</p>
        </div>
        <div className="project__item__delete__buttons">
          <button
            className="delete-project__btn"
            onClick={() => setDeleteMode(false)}
          >
            Cancel
          </button>
          <button
            className="delete-project__btn danger"
            onClick={() => deleteProject()}
          >
            {submitLoading ? "Deleting ..." : "Delete"}
          </button>
        </div>
      </div>
    </div>
  );
};

const ProjectItem = ({ project, idx }) => {
  const iconProps = {
    width: 18,
    height: 18,
    strokeWidth: 2,
  };

  const { setEditProjectMode, deleteMode, setDeleteMode } =
    useContext(ProfileContext);

  return (
    <div className="projects__list__item">
      {deleteMode && deleteMode.project == idx && (
        <DeleteProjectModal projectIndex={idx} />
      )}
      <h3 className="project__item__name">{project.name}</h3>
      <p className="project__item__description">{project.description}</p>
      <div className="project__item__buttons">
        <button
          onClick={() => setEditProjectMode(project)}
          type="button"
          className="project__item__edit-btn"
        >
          <EditPencil strokeWidth={2} />
        </button>
        <button
          onClick={() => setDeleteMode({ project: idx })}
          type="button"
          className="project__item__edit-btn"
        >
          <Trash strokeWidth={2} />
        </button>
      </div>
      <ul className="project__item__metadata">
        <IconoirProvider iconProps={iconProps}>
          <li className="project__metadata__pill">
            <Calendar width={16} height={16} />
            <span>{project.year}</span>
          </li>
          <li className="project__metadata__pill">
            <GithubCircle />
            <a href={project.repo}>repo</a>
          </li>
          <li className="project__metadata__pill">
            <Link />
            <a href={project.url}>visit</a>
          </li>
        </IconoirProvider>
      </ul>
    </div>
  );
};

const ProjectsList = ({ projects }) => {
  return (
    <div className="projects__list">
      {projects &&
        projects.map((project, idx) => (
          <ProjectItem key={idx} project={project} idx={idx} />
        ))}
    </div>
  );
};

ProjectItem.propTypes = {
  project: PropTypes.object,
  idx: PropTypes.number,
};

ProjectsList.propTypes = {
  projects: PropTypes.array,
};

DeleteProjectModal.propTypes = {
  projectIndex: PropTypes.number,
};

export default ProjectsList;
