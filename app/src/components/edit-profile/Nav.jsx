import { NavArrowDown } from "iconoir-react";
import { useContext, useState } from "react";
import { ProfileContext } from "../../context/ProfileContext";

const Nav = () => {
  const { visibleForm, toggleForm } = useContext(ProfileContext);
  const [isOpen, setIsOpen] = useState(false);

  const buttons = [
    { name: "Profile", className: "profile-btn", new: false },
    { name: "Work", className: "work", new: true },
    { name: "Social", className: "social", new: false },
    { name: "Contact", className: "contact", new: false },
    { name: "Projects", className: "projects", new: false },
  ];

  return (
    <nav className="protected__nav">
      <ul
        className={`protected__nav__links ${isOpen ? "open" : undefined}`}
        onClick={() => setIsOpen(!isOpen)}
      >
        <NavArrowDown
          className={`protected__nav__arrow ${isOpen ? "nav-open" : undefined}`}
          width={20}
          height={20}
          strokeWidth={2.2}
        />
        {buttons.map((btn, idx) => (
          <li
            className={`protected__nav__item ${
              visibleForm === idx + 1 ? "active" : ""
            } ${btn.className}`}
            onClick={() => toggleForm(idx + 1)}
            key={idx}
          >
            <span className="protected__nav__item__name">
              {btn.name}
              {btn.new && (
                <span className="protected__nav__pill new-pill">new</span>
              )}
            </span>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Nav;
