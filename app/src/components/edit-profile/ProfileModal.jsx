import { v4 as uuidv4 } from "uuid";
import {
  ArrowLeft,
  DeleteCircle,
  RefreshDouble,
  SaveFloppyDisk,
} from "iconoir-react";
import { useContext, useEffect } from "react";
import { useForm } from "react-hook-form";
import { ProfileContext } from "../../context/ProfileContext";
import FormPill from "../common/FormPill";

const ProfileModal = () => {
  const { modal, closeModal, submitLoading, updateProfile, profile } =
    useContext(ProfileContext);
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    ...(modal.type === "edit" && { defaultValues: modal.project }),
  });

  const onSubmit = async (data) => {
    // NOTE: if we're in edit mode, we simply replace
    // the project with the new data and submit as usual
    // this is until we move to RDS and have a specific table for projects
    // that we can update separately from the profile
    if (modal.type === "edit") {
      const index = profile.projects.indexOf(modal.project);
      profile.projects[index] = data;
    } else {
      // NOTE: we're in create mode which means we need to generate a UUID
      // before sending it to the server
      data.uuid = uuidv4();
      profile.projects.push(data);
    }
    await updateProfile(profile, reset);
    closeModal();
  };

  useEffect(() => {
    const close = (e) => {
      if (e.key === "Escape") {
        closeModal();
      }
    };
    window.addEventListener("keydown", close);
    return () => window.removeEventListener("keydown", close);
  }, [closeModal]);

  console.log(modal.project);

  return (
    <section className="project__modal">
      <div className="project__modal__container">
        <button onClick={closeModal} className="project__modal__back-btn">
          <ArrowLeft width={22} height={22} strokeWidth={3} />
        </button>

        <header className="project__modal__header">
          <h2 className="project__modal__title">
            {modal.type == "edit" ? (
              <>Editing : {modal.project.name}</>
            ) : (
              <>Add a new project</>
            )}
          </h2>
          <div className="project__modal__subtitle">
            <p>Your personal / side projects will appear on your profile.</p>
            <p>Pick the work you're most proud of !</p>
          </div>
        </header>

        <form
          className="project__modal__form"
          onSubmit={handleSubmit(onSubmit)}
        >
          <fieldset className="form__field-group">
            <div className="form__field">
              <label htmlFor="name" className="field__label">
                <span className="field__label__text">
                  <span className={errors.name && "field__label--error"}>
                    Project name
                  </span>
                  {errors.name && <FormPill type="error" message="required" />}
                </span>
                <input
                  type="text"
                  name="name"
                  placeholder="ngnr.club"
                  className={`field__input ${errors.name && "error"}`}
                  {...register("name", { required: true })}
                />
              </label>
            </div>
            <div className="form__field">
              <label htmlFor="year" className="field__label">
                <span className="field__label__text">
                  <span className={errors.year && "field__label--error"}>
                    Year
                  </span>
                  {errors.year && <FormPill type="error" message="required" />}
                </span>
                <input
                  type="text"
                  name="year"
                  placeholder="2022"
                  className={`field__input ${errors.year && "error"}`}
                  {...register("year")}
                />
              </label>
            </div>
          </fieldset>
          <fieldset className="form__field-group">
            <div className="form__field">
              <label htmlFor="url" className="field__label">
                <span className="field__label__text">
                  <span className={errors.url && "field__label--error"}>
                    Project URL
                  </span>
                  {errors.url && <FormPill type="error" message="required" />}
                </span>
                <input
                  type="text"
                  name="url"
                  placeholder="https://ngnr.club"
                  className={`field__input ${errors.url && "error"}`}
                  {...register("url", { required: true })}
                />
              </label>
            </div>
            <div className="form__field">
              <label htmlFor="repo" className="field__label">
                <span className="field__label__text">
                  <span className={errors.repo && "field__label--error"}>
                    Repo URL
                  </span>
                  {errors.repo && <FormPill type="error" message="required" />}
                </span>
                <input
                  type="text"
                  name="repo"
                  placeholder="https://gitlab.com/fishrxyz/ngnr.club"
                  className={`field__input ${errors.repo && "error"}`}
                  {...register("repo")}
                />
              </label>
            </div>
          </fieldset>
          <div className="form__field">
            <label htmlFor="description" className="field__label">
              <span className="field__label__text">
                <span className={errors.description && "field__label--error"}>
                  About this project
                </span>
                {errors.description && (
                  <FormPill type="error" message="required" />
                )}
              </span>
              <textarea
                name="description"
                placeholder="Elevator pitch for the project."
                className={`field__input ${errors.description && "error"}`}
                {...register("description", { required: true })}
              />
            </label>
          </div>
          <fieldset className="form__field-group">
            <button
              className="project__modal__btn project__modal__btn--cancel"
              onClick={closeModal}
              type="button"
            >
              <DeleteCircle width={20} height={20} strokeWidth={2} />
              <span>Take me back</span>
            </button>
            <button
              className={`project__modal__btn project__modal__btn--submit ${
                submitLoading && "loading"
              }`}
              type="submit"
            >
              {submitLoading ? (
                <>
                  <RefreshDouble width={20} height={20} strokeWidth={2.2} />
                  <span>Saving ...</span>
                </>
              ) : (
                <>
                  <SaveFloppyDisk width={20} height={20} strokeWidth={2.2} />
                  <span>Save project</span>
                </>
              )}
            </button>
          </fieldset>
        </form>
      </div>
    </section>
  );
};

export default ProfileModal;
