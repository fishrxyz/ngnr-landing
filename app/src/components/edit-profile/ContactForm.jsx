import {
  Discord,
  JournalPage,
  KeyAlt,
  Mail,
  Medal1St,
  PineTree,
  RefreshDouble,
  SaveFloppyDisk,
} from "iconoir-react";
import { useContext } from "react";
import { useForm } from "react-hook-form";
import { ProfileContext } from "../../context/ProfileContext";

const ContactForm = () => {
  const { visibleForm, submitLoading, updateProfile, profile } =
    useContext(ProfileContext);

  const { register, handleSubmit, reset } = useForm({ defaultValues: profile });

  const onSubmit = async (data) => {
    await updateProfile(data, reset);
  };

  return (
    <div className={`protected__form ${visibleForm === 4 && "visible"}`}>
      <form onSubmit={handleSubmit(onSubmit)} className="protected__form__form">
        <header className="protected__form__header">
          <h3 className="protected__form__title">Contact info and stuff</h3>
          <p className="protected__form__subtitle">
            Email, certs and anything else that might be of interest.
          </p>
        </header>
        <div className="form__field">
          <label htmlFor="email" className="field__label">
            <span className="field__label__text">Contact Email</span>
            <div className="field__input__wrapper">
              <i className="field__input__icon">
                <Mail />
              </i>
              <input
                type="text"
                name="email"
                placeholder="ex: contact@ngnr.club"
                className="field__input"
                {...register("email")}
              />
            </div>
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="discord" className="field__label">
            <span className="field__label__text">Discord</span>
            <div className="field__input__wrapper">
              <i className="field__input__icon">
                <Discord />
              </i>
              <input
                type="text"
                name="discord"
                placeholder="ex: ayofishr#8326"
                className="field__input"
                {...register("discord")}
              />
            </div>
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="blog" className="field__label">
            <span className="field__label__text">Blog</span>
            <div className="field__input__wrapper">
              <i className="field__input__icon">
                <JournalPage />
              </i>
              <input
                type="text"
                name="blog"
                placeholder="ex: https://0xfishr.xyz/notes"
                className="field__input"
                {...register("blog")}
              />
            </div>
          </label>
        </div>

        <div className="form__field">
          <label htmlFor="now_page" className="field__label">
            <span className="field__label__text">Now Page</span>
            <div className="field__input__wrapper">
              <i className="field__input__icon">
                <PineTree />
              </i>
              <input
                type="text"
                name="now_page"
                placeholder="ex: https://0xfishr.xyz/now"
                className="field__input"
                {...register("now_page")}
              />
            </div>
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="keybase" className="field__label">
            <span className="field__label__text">Keybase</span>
            <div className="field__input__wrapper">
              <i className="field__input__icon">
                <KeyAlt />
              </i>
              <input
                type="text"
                name="keybase"
                placeholder="ex: https://keybase.com/ayofishr"
                className="field__input"
                {...register("keybase")}
              />
            </div>
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="credly" className="field__label">
            <span className="field__label__text">Credly Badges</span>
            <div className="field__input__wrapper">
              <i className="field__input__icon">
                <Medal1St />
              </i>
              <input
                type="text"
                name="credly"
                placeholder="ex: https://www.credly.com/users/karim-cheurfi/badges"
                className="field__input"
                {...register("credly")}
              />
            </div>
          </label>
        </div>
        <button
          type="submit"
          className={`protected__form__submit ${submitLoading && "loading"}`}
        >
          {submitLoading ? (
            <>
              <RefreshDouble width={20} height={20} strokeWidth={2.2} />
              <span>Saving ...</span>
            </>
          ) : (
            <>
              <SaveFloppyDisk width={20} height={20} strokeWidth={2.2} />
              <span>Save info</span>
            </>
          )}
        </button>
      </form>
    </div>
  );
};

export default ContactForm;
