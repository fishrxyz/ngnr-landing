import { PeaceHand } from "iconoir-react";
import { useAuth } from "../../hooks/useAuth";

const Header = () => {
  const {
    user: { preferred_username },
  } = useAuth();

  return (
    <header className="edit-profile__header">
      <h2 className="edit-profile__greeting">
        <span>
          Welcome back,{" "}
          <span className="greeting__username">@{preferred_username}</span>
        </span>
        <PeaceHand width={35} height={35} />
      </h2>
      <p className="edit-profile__blurb">
        This is your home base. Edit your profile, add your photo, manage your
        links ...
      </p>
    </header>
  );
};

export default Header;
