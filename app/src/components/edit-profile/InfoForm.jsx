import { RefreshDouble, SaveFloppyDisk } from "iconoir-react";
import { useContext } from "react";
import { useForm, useWatch } from "react-hook-form";
import { ProfileContext } from "../../context/ProfileContext";
import FormPill from "../common/FormPill";
import AvatarForm from "./AvatarForm";

const InfoForm = () => {
  const { visibleForm, submitLoading, updateProfile, username, profile } =
    useContext(ProfileContext);

  const {
    register,
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm({ defaultValues: profile });

  const intToBool = (str) => Boolean(parseInt(str, 10));
  const onSubmit = async (data) => {
    data.freelance = intToBool(data.freelance);
    await updateProfile(data, reset);
  };

  const bioValue = useWatch({ control, name: "bio" });
  const bioMaxLength = 150;
  const remainingChars = bioMaxLength - bioValue.length;

  return (
    <div className={`protected__form ${visibleForm === 1 && "visible"}`}>
      <AvatarForm />
      <form className="protected__form__form" onSubmit={handleSubmit(onSubmit)}>
        <header className="protected__form__header">
          <h3 className="profile__form__title">About Yourself</h3>
          <p className="profile__form__subtitle">
            Who is @{username} really ? The world wants to know !
          </p>
        </header>
        <div className="form__field">
          <label htmlFor="display_name" className="field__label">
            <span className="field__label__text">
              <span className={errors.display_name && "field__label--error"}>
                What&apos;s your name ?
              </span>
              {errors.display_name && (
                <FormPill type="error" message="required" />
              )}
            </span>
            <input
              type="text"
              name="display_name"
              placeholder="Karim Benzema"
              className={`field__input ${errors.display_name && "error"}`}
              {...register("display_name", { required: true })}
            />
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="location" className="field__label">
            <span className="field__label__text">
              <span className={errors.location && "field__label--error"}>
                Where are you located ?
              </span>
              {errors.location && <FormPill type="error" message="required" />}
            </span>
            <input
              type="text"
              placeholder="Madrid, Spain"
              name="location"
              className={`field__input ${errors.location && "error"}`}
              {...register("location", { required: true })}
            />
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="position" className="field__label">
            <span className="field__label__text">
              <span className={errors.position && "field__label--error"}>
                What is your current role ?
              </span>
              {errors.position && <FormPill type="error" message="required" />}
            </span>
            <input
              type="text"
              name="position"
              placeholder="ex: Cloud engineer, Software developer ..."
              className={`field__input ${errors.position && "error"}`}
              {...register("position", { required: true })}
            />
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="bio" className="field__label">
            <span className="field__label__text">
              <span className={errors.bio && "field__label--error"}>
                Bio (max: 150 chars)
              </span>
              {errors.bio && <FormPill type="error" message="required" />}
            </span>
            <textarea
              type="text"
              name="bio"
              maxLength={bioMaxLength}
              placeholder="Make it short. Make it POP. This is your chance to add a bit of personality to your profile."
              className={`field__input ${errors.bio && "error"}`}
              {...register("bio", { required: true })}
            />
            <p className={`field__input__data-binding`}>
              <span>
                chars left:{" "}
                <strong className={remainingChars < 20 ? "danger" : undefined}>
                  {bioMaxLength - bioValue.length}
                </strong>
              </span>
            </p>
          </label>
        </div>
        <button
          type="submit"
          className={`protected__form__submit ${submitLoading && "loading"}`}
        >
          {submitLoading ? (
            <>
              <RefreshDouble width={20} height={20} strokeWidth={2.2} />
              <span>Saving ...</span>
            </>
          ) : (
            <>
              <SaveFloppyDisk width={20} height={20} strokeWidth={2.2} />
              <span>Save profile</span>
            </>
          )}
        </button>
      </form>
    </div>
  );
};

export default InfoForm;
