import {
  Codepen,
  GitHub,
  GitLabFull,
  Globe,
  RefreshDouble,
  SaveFloppyDisk,
  Twitter,
  YouTube,
} from "iconoir-react";
import { useContext } from "react";
import { useForm } from "react-hook-form";
import Twitch from "../../assets/icons/Twitch";
import { ProfileContext } from "../../context/ProfileContext";

const SocialForm = () => {
  const { visibleForm, submitLoading, updateProfile, profile } =
    useContext(ProfileContext);

  const { register, handleSubmit, reset } = useForm({ defaultValues: profile });

  const onSubmit = async (data) => {
    await updateProfile(data, reset);
  };

  return (
    <div className={`protected__form ${visibleForm === 3 && "visible"}`}>
      <form onSubmit={handleSubmit(onSubmit)} className="protected__form__form">
        <header className="protected__form__header">
          <h3 className="protected__form__title">Social Media</h3>
          <p className="protected__form__subtitle">
            Where can people find you on the web ?
          </p>
        </header>

        <div className="form__field">
          <label htmlFor="twitch" className="field__label">
            <span className="field__label__text">Twitch</span>
            <div className="field__input__wrapper">
              <i className="field__input__icon">
                <Twitch />
              </i>
              <input
                type="text"
                name="twitch"
                placeholder="ex: https://twitch.tv/ayofishr"
                className="field__input"
                {...register("twitch")}
              />
            </div>
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="twitter" className="field__label">
            <span className="field__label__text">Twitter</span>
            <div className="field__input__wrapper">
              <i className="field__input__icon">
                <Twitter />
              </i>
              <input
                type="text"
                name="twitter"
                placeholder="ex: https://twitter.com/ayofishr"
                className="field__input"
                {...register("twitter")}
              />
            </div>
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="youtube" className="field__label">
            <span className="field__label__text">Youtube</span>
            <div className="field__input__wrapper">
              <i className="field__input__icon">
                <YouTube />
              </i>
              <input
                type="text"
                name="youtube"
                placeholder="ex: https://youtube.com/@ayofishr"
                className="field__input"
                {...register("youtube")}
              />
            </div>
          </label>
        </div>

        <div className="form__field">
          <label htmlFor="codepen" className="field__label">
            <span className="field__label__text">Codepen</span>
            <div className="field__input__wrapper">
              <i className="field__input__icon">
                <Codepen />
              </i>
              <input
                type="text"
                name="codepen"
                placeholder="ex: https://codepen.com/fishrxyz"
                className="field__input"
                {...register("codepen")}
              />
            </div>
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="gitlab" className="field__label">
            <span className="field__label__text">Gitlab</span>
            <div className="field__input__wrapper">
              <i className="field__input__icon">
                <GitLabFull />
              </i>
              <input
                type="text"
                name="gitlab"
                placeholder="ex: https://gitlab.com/fishrxyz"
                className="field__input"
                {...register("gitlab")}
              />
            </div>
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="github" className="field__label">
            <span className="field__label__text">Github</span>
            <div className="field__input__wrapper">
              <i className="field__input__icon">
                <GitHub />
              </i>
              <input
                type="text"
                name="github"
                placeholder="ex: https://github.com/fishrxyz"
                className="field__input"
                {...register("github")}
              />
            </div>
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="lobsters" className="field__label">
            <span className="field__label__text">Lobster.rs</span>
            <div className="field__input__wrapper">
              <i className="field__input__icon">
                <Globe />
              </i>
              <input
                type="text"
                name="lobsters"
                placeholder="ex: https://lobster.rs/ayofishr"
                className="field__input"
                {...register("lobsters")}
              />
            </div>
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="hn" className="field__label">
            <span className="field__label__text">HackerNews handle</span>
            <div className="field__input__wrapper">
              <i className="field__input__icon">
                <Globe />
              </i>
              <input
                type="text"
                name="hn"
                placeholder="ex: ayofishr"
                className="field__input"
                {...register("hn")}
              />
            </div>
          </label>
        </div>
        <button
          type="submit"
          className={`protected__form__submit ${submitLoading && "loading"}`}
        >
          {submitLoading ? (
            <>
              <RefreshDouble width={20} height={20} strokeWidth={2.2} />
              <span>Saving ...</span>
            </>
          ) : (
            <>
              <SaveFloppyDisk width={20} height={20} strokeWidth={2.2} />
              <span>Save links</span>
            </>
          )}
        </button>
      </form>
    </div>
  );
};

export default SocialForm;
