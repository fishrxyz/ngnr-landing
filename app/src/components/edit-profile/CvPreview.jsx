import PropTypes from "prop-types";
import {
  BinMinus,
  CloudDownload,
  EyeEmpty,
  Page,
  RefreshDouble,
} from "iconoir-react";
import { useContext } from "react";
import axios from "redaxios";
import { ProfileContext } from "../../context/ProfileContext";
import { useAuth } from "../../hooks/useAuth";

const CvPreview = ({ url, file, isUploading }) => {
  const {
    user: { preferred_username },
  } = useAuth();

  const { updateCvURL } = useContext(ProfileContext);

  const deleteCV = async (e) => {
    e.preventDefault();
    await updateCvURL("");
  };

  const downloadCV = async (e, url, filename) => {
    e.preventDefault();
    const res = await axios.get(url, {
      responseType: "blob",
      headers: {
        "Content-Type": "application/pdf",
      },
    });
    const blob = new Blob([res.data]);
    const downloadUrl = window.URL.createObjectURL(blob);

    // Create URL
    const link = document.createElement("a");
    link.href = downloadUrl;
    link.setAttribute("download", filename);

    // Append link to body
    document.body.appendChild(link);

    // Start download
    link.click();

    // Clean up and remove the link
    link.parentNode.removeChild(link);
  };

  return (
    <section>
      <h3 className="form__subtitle">
        {isUploading ? "Uploading Resume ..." : "Your CV / Resume"}
      </h3>
      <div className={`file-upload__preview cv-preview`}>
        {isUploading ? (
          <RefreshDouble
            className="uploading-cv"
            width={20}
            height={20}
            strokeWidth={2}
          />
        ) : (
          <Page width={20} height={20} strokeWidth={2} />
        )}
        <span className="cv-preview__name">
          {isUploading ? `${file.name}` : `resume.pdf`}
        </span>
        {isUploading && (
          <span className="cv-preview__size">{file.size} bytes</span>
        )}
        {!isUploading && (
          <div className="cv-preview__buttons">
            <a
              href={url}
              target="_blank"
              className={`cv-preview__btn`}
              rel="noreferrer"
            >
              <EyeEmpty strokeWidth={2} />
            </a>
            <button
              className="cv-preview__btn"
              onClick={(e) =>
                downloadCV(e, url, `${preferred_username}-resume.pdf`)
              }
            >
              <CloudDownload strokeWidth={2} />
            </button>
            <button className="cv-preview__btn">
              <BinMinus onClick={(e) => deleteCV(e)} strokeWidth={2} />
            </button>
          </div>
        )}
      </div>
    </section>
  );
};

CvPreview.propTypes = {
  url: PropTypes.string,
  file: PropTypes.object,
  isUploading: PropTypes.bool,
};

export default CvPreview;
