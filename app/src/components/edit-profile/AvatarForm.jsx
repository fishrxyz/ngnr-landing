import { RefreshDouble, Upload } from "iconoir-react";
import { useContext, useState } from "react";
import { AVATAR_PRESIGNED_URL_ENDPOINT } from "../../config/constants";
import { ProfileContext } from "../../context/ProfileContext";

const AvatarForm = () => {
  const {
    visibleForm,
    requestPresignedURL,
    uploadFileToS3,
    updateAvatarURL,
    profile,
  } = useContext(ProfileContext);

  const [file, setFile] = useState(null);
  const [isUploading, setIsUploading] = useState(false);

  const handleFileChange = (e) => {
    if (e.target.files) {
      setFile(e.target.files[0]);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!file) {
      return;
    }
    setIsUploading(true);

    // request a presigned URL
    const presignedURL = await requestPresignedURL(
      AVATAR_PRESIGNED_URL_ENDPOINT,
    );

    const headers = {
      "x-amz-acl": "public-read",
    };

    // upload the file
    // get the uploded image's URL
    const imageURL = await uploadFileToS3(presignedURL, file, headers);

    await updateAvatarURL(imageURL);
    setIsUploading(false);
    setFile(null);
  };

  const avatarURL = profile.avatar
    ? profile.avatar
    : "https://cdn.dribbble.com/assets/avatar-default-e370af14535cdbf137637a27ee1a8e451253edc80be429050bc29d59b1f7cda0.gif";

  return (
    <section className="avatar-form">
      <form
        className={`edit-profile__form avatar-form ${
          visibleForm === 1 && "visible"
        }`}
        onSubmit={handleSubmit}
      >
        <span className="avatar-form__title">Choose a picture</span>
        <div className="avatar-form__container">
          <figure
            className="avatar-form__pic avatar-form__flex-item"
            style={{
              backgroundImage: `url(${avatarURL})`,
            }}
          ></figure>
          <div className="avatar-form__group avatar-form__flex-item">
            <input
              onChange={handleFileChange}
              type="file"
              name="avatar"
              accept="image/*"
            />
            <button
              disabled={file === null}
              className={`avatar-form__submit ${isUploading && "uploading"}`}
              type="submit"
            >
              {!isUploading ? (
                <>
                  <Upload width={18} height={18} strokeWidth={2.2} />
                  <span>Upload profile picture</span>
                </>
              ) : (
                <>
                  <RefreshDouble width={20} height={20} strokeWidth={2.2} />
                  <span>Uploading ...</span>
                </>
              )}
            </button>
          </div>
        </div>
        <div className="avatar-form__tooltip avatar-form__flex-item">
          <p>
            <strong>Pro Tip:</strong> Let the world put a face to the name !
            Choose a picture where you face is clearly visible with a plain
            background.
          </p>
        </div>
      </form>
    </section>
  );
};

export default AvatarForm;
