import { useContext, useRef, useState } from "react";
import { useForm, useWatch, Controller } from "react-hook-form";
import { ProfileContext } from "../../context/ProfileContext";
import * as Switch from "@radix-ui/react-switch";
import FormPill from "../common/FormPill";
import { CloudUpload, RefreshDouble, SaveFloppyDisk } from "iconoir-react";
import {
  CV_MAX_FILE_SIZE,
  CV_PRESIGNED_URL_ENDPOINT,
} from "../../config/constants";
import CvPreview from "./CvPreview";

const WorkPreferencesForm = () => {
  const {
    visibleForm,
    requestPresignedURL,
    submitLoading,
    updateCvURL,
    updateProfile,
    uploadFileToS3,
    setSubmitError,
    profile,
  } = useContext(ProfileContext);

  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm({ defaultValues: profile.work_preferences });

  const intToBool = (value) => Boolean(parseInt(value));
  const strToInt = (value) => parseInt(value);
  const workAvailability = useWatch({
    control,
    name: "freelance",
    defaultValue: Number(profile.work_preferences.freelance),
  });
  const isFreelance = intToBool(workAvailability);

  // NOTE: CV related variables
  const cvRef = useRef(null);
  const [file, setFile] = useState(null);
  const [isUploading, setIsUploading] = useState(false);

  const openFileBrowser = () => {
    if (file !== null) {
      return;
    }
    cvRef.current.click();
  };

  const onFileChange = async (e) => {
    const fileToUpload = e.target.files[0];
    setFile(fileToUpload);
    setIsUploading(true);

    // check if file size is bigger than max cv file size
    if (fileToUpload.size > CV_MAX_FILE_SIZE) {
      setIsUploading(false);
      setSubmitError({ msg: "CV is over 3MB" });
      setFile(null);
    }

    const headers = {
      "x-amz-acl": "public-read",
      "Content-Type": "application/pdf",
      "Content-Length": fileToUpload.size,
    };

    const fd = new FormData();
    fd.append("Content-Type", "application/pdf");
    fd.append("file", fileToUpload);

    // request a presigned URL
    const presignedURL = await requestPresignedURL(CV_PRESIGNED_URL_ENDPOINT);

    const cvURL = await uploadFileToS3(presignedURL, fd, headers);

    await updateCvURL(cvURL);
    setIsUploading(false);
    setFile(null);
  };
  const onSubmit = async (data) => {
    data.freelance = intToBool(data.freelance);
    profile.work_preferences = data;
    await updateProfile(profile);
  };

  return (
    <div className={`protected__form ${visibleForm === 2 && "visible"}`}>
      <form className="protected__form__form" onSubmit={handleSubmit(onSubmit)}>
        <header className="protected__form__header">
          <h3 className="profile__form__title">Work Preferences</h3>
          <p className="profile__form__subtitle">
            Set your availability, upload your CV and set your rates.
          </p>
        </header>

        <section className="protected__form__subsection">
          <h3 className="form__subtitle">What is your work availability ?</h3>

          <fieldset className="protected__form__group">
            <div className="form__field">
              <label htmlFor="freelance" className="field__label radio">
                <input
                  type="radio"
                  className="form__radio__custom"
                  name="freelance"
                  value={1}
                  defaultChecked={profile.work_preferences.freelance === true}
                  {...register("freelance", {
                    setValueAs: strToInt,
                    required: true,
                  })}
                />
                <div className="form__radio__content">
                  <h3>Freelance</h3>
                  <p>You choose the flexibility afforded by contract work</p>
                </div>
              </label>
            </div>

            <div className="form__field">
              <label htmlFor="freelance" className="field__label radio">
                <input
                  type="radio"
                  name="freelance"
                  className="form__radio__custom"
                  value={0}
                  defaultChecked={profile.work_preferences.freelance === false}
                  {...register("freelance", {
                    setValueAs: strToInt,
                    required: true,
                  })}
                />
                <div className="form__radio__content">
                  <h3>Full time</h3>
                  <p>You prefer the stability of a full time role</p>
                </div>
              </label>
            </div>
          </fieldset>
        </section>

        {isFreelance === true && (
          <section className="protected__form__subsection">
            <h3 className="form__subtitle">
              What are your current rates ? (in USD)
            </h3>

            <fieldset className="protected__form__group">
              <div className="form__field third">
                <label htmlFor="freelance_min_rate" className="field__label">
                  <span className="field__label__text">
                    <span
                      className={
                        errors.freelance_min_rate && "field__label--error"
                      }
                    >
                      Minimum Hourly Rate
                    </span>
                    {errors.freelance_min_rate && (
                      <FormPill type="error" message="required" />
                    )}
                  </span>
                  <input
                    type="number"
                    min={0}
                    placeholder="$"
                    className={`field__input ${
                      errors.freelance_min_rate && "error"
                    }`}
                    name="freelance_min_rate"
                    defaultValue={profile.work_preferences.freelance_min_rate}
                    {...register("freelance_min_rate", {
                      setValueAs: strToInt,
                      required: true,
                    })}
                  />
                </label>
              </div>

              <div className="form__field third">
                <label htmlFor="freelance_fixed_rate" className="field__label">
                  <span className="field__label__text">
                    <span
                      className={
                        errors.freelance_fixed_rate && "field__label--error"
                      }
                    >
                      Min Fixed Price Budget
                    </span>
                    {errors.freelance_fixed_rate && (
                      <FormPill type="error" message="required" />
                    )}
                  </span>
                  <input
                    type="number"
                    min={0}
                    placeholder="$"
                    className={`field__input ${
                      errors.freelance_fixed_rate && "error"
                    }`}
                    name="freelance_fixed_rate"
                    defaultValue={profile.work_preferences.freelance_fixed_rate}
                    {...register("freelance_fixed_rate", {
                      setValueAs: strToInt,
                      required: true,
                    })}
                  />
                </label>
              </div>

              <div className="form__field third">
                <label htmlFor="freelance_min_hours" className="field__label">
                  <span className="field__label__text">
                    <span
                      className={
                        errors.freelance_min_hours && "field__label--error"
                      }
                    >
                      Min hours / contract
                    </span>
                  </span>
                  <input
                    type="number"
                    min={0}
                    placeholder="$"
                    className={`field__input ${
                      errors.freelance_min_hours && "error"
                    }`}
                    name="freelance_min_hours"
                    defaultValue={profile.work_preferences.freelance_min_hours}
                    {...register("freelance_min_hours", {
                      setValueAs: strToInt,
                      required: true,
                    })}
                  />
                </label>
                {errors.freelance_min_hours && (
                  <FormPill type="error" message="required" />
                )}
              </div>
            </fieldset>
          </section>
        )}
        {isFreelance === false && (
          <section className="protected__form__subsection">
            <h3 className="form__subtitle">
              What's your preferred salary range ? (in USD)
            </h3>

            <fieldset className="protected__form__group">
              <div className="form__field">
                <label htmlFor="salary_min" className="field__label">
                  <span className="field__label__text">
                    <span
                      className={errors.salary_min && "field__label--error"}
                    >
                      Minimum
                    </span>
                    {errors.salary_min && (
                      <FormPill type="error" message="required" />
                    )}
                  </span>
                  <input
                    type="number"
                    min={0}
                    placeholder="$"
                    className={`field__input ${errors.salary_min && "error"}`}
                    name="salary_min"
                    defaultValue={profile.work_preferences.salary_min}
                    {...register("salary_min", {
                      setValueAs: strToInt,
                      required: true,
                    })}
                  />
                </label>
              </div>

              <div className="form__field">
                <label htmlFor="salary_max" className="field__label">
                  <span className="field__label__text">
                    <span
                      className={errors.salary_max && "field__label--error"}
                    >
                      Maximum
                    </span>
                    {errors.salary_max && (
                      <FormPill type="error" message="required" />
                    )}
                  </span>
                  <input
                    type="number"
                    min={0}
                    placeholder="$"
                    className={`field__input ${errors.salary_max && "error"}`}
                    name="salary_max"
                    defaultValue={profile.work_preferences.salary_max}
                    {...register("salary_max", {
                      setValueAs: strToInt,
                      required: true,
                    })}
                  />
                </label>
              </div>
            </fieldset>
          </section>
        )}

        <section className="protected__form__subsection">
          <h3 className="form__subtitle">Boring yet important legal stuff</h3>
          <fieldset className="protected__form__group protected__form__group--options">
            <div className="form__field">
              <h4>
                <span>🇺🇸</span> Do you have a US Visa ?
              </h4>
              <Controller
                control={control}
                name="us_ok"
                render={({ field }) => (
                  <Switch.Root
                    defaultChecked={profile.work_preferences.us_ok}
                    name={field.name}
                    onCheckedChange={field.onChange}
                    className="toggle-switch"
                  >
                    <Switch.Thumb className="toggle-switch__thumb" />
                  </Switch.Root>
                )}
              />
            </div>
            <div className="form__field">
              <h4>
                <span>🇪🇺</span> Can you work in the EU ?
              </h4>
              <Controller
                control={control}
                name="eu_ok"
                render={({ field }) => (
                  <Switch.Root
                    defaultChecked={profile.work_preferences.eu_ok}
                    name={field.name}
                    onCheckedChange={field.onChange}
                    className="toggle-switch"
                  >
                    <Switch.Thumb className="toggle-switch__thumb" />
                  </Switch.Root>
                )}
              />
            </div>
            <div className="form__field">
              <h4>
                <span>🌍</span> Can you work remotely ?
              </h4>
              <Controller
                control={control}
                name="remote_ok"
                render={({ field }) => (
                  <Switch.Root
                    defaultChecked={profile.work_preferences.remote_ok}
                    name={field.name}
                    onCheckedChange={field.onChange}
                    className="toggle-switch"
                  >
                    <Switch.Thumb className="toggle-switch__thumb" />
                  </Switch.Root>
                )}
              />
            </div>
          </fieldset>
        </section>

        <section className="protected__form__subsection">
          {profile.cv.url === "" ? (
            <>
              <h3 className="form__subtitle">Upload your CV / Resume </h3>
              <div
                className={`protected__form__file-upload ${
                  file !== null ? "disabled" : undefined
                }`}
              >
                <div className="file-upload__container">
                  <CloudUpload
                    className="file-upload__icon"
                    width={50}
                    height={50}
                    strokeWidth={2}
                  />
                  <p className="file-upload__text">
                    <strong
                      onClick={openFileBrowser}
                      className={`file-upload__link ${
                        file !== null ? "disabled" : undefined
                      }`}
                    >
                      Click here
                    </strong>{" "}
                    to select a file
                  </p>
                  <p className="file-upload__text mini">Max 3MB - PDF only</p>
                </div>
                <input
                  type="file"
                  accept="application/pdf"
                  ref={cvRef}
                  name="cv_file"
                  onChange={onFileChange}
                  style={{ display: "none" }}
                />
              </div>
              {isUploading && (
                <CvPreview
                  url={profile.cv.url}
                  file={file}
                  isUploading={isUploading}
                />
              )}
            </>
          ) : (
            <CvPreview
              url={profile.cv.url}
              file={file}
              isUploading={isUploading}
            />
          )}
        </section>

        <button
          type="submit"
          className={`protected__form__submit ${submitLoading && "loading"}`}
        >
          {submitLoading ? (
            <>
              <RefreshDouble width={20} height={20} strokeWidth={2.2} />
              <span>Saving Preferences</span>
            </>
          ) : (
            <>
              <SaveFloppyDisk width={20} height={20} strokeWidth={2.2} />
              <span>Save Preferences</span>
            </>
          )}
        </button>
      </form>
    </div>
  );
};

export default WorkPreferencesForm;
