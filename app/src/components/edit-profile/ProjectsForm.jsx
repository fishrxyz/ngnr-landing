import { AddCircle } from "iconoir-react";
import { useContext } from "react";
import { ProfileContext } from "../../context/ProfileContext";
import ProjectModal from "./ProfileModal";
import ProjectsList from "./ProjectsList";

const ProjectsForm = () => {
  const { visibleForm, profile, setAddProjectMode, modal } =
    useContext(ProfileContext);

  return (
    <>
      <div className={`protected__form ${visibleForm === 5 && "visible"}`}>
        <header className="protected__form__header">
          <h3 className="protected__form__title">Personal projects</h3>
          <p className="protected__form__subtitle">
            Got any interesting side projects you're working on ? Share them !{" "}
          </p>
        </header>

        {profile.projects.length === 0 ? (
          <div className="protected__form__notice normal">
            <h3 className="protected__form__notice__title">
              Start by adding a project
            </h3>
            <p>
              Listing personal projects will make your profile look more
              appealing and increase your chances of getting contacted !
            </p>
            <button
              onClick={setAddProjectMode}
              type="button"
              className="protected__form__notice__button"
            >
              <AddCircle width={20} height={20} strokeWidth={2.2} />
              <span>New project</span>
            </button>
          </div>
        ) : (
          <>
            <ProjectsList projects={profile.projects} />
            <button
              type="button"
              className="protected__form__submit profile-submit"
              onClick={setAddProjectMode}
            >
              <AddCircle width={20} height={20} strokeWidth={2.2} />
              <span>Add Project</span>
            </button>
          </>
        )}
        {modal && (
          <ProjectModal
            project={modal.type === "edit" ? modal.project : null}
          />
        )}
      </div>
    </>
  );
};

export default ProjectsForm;
