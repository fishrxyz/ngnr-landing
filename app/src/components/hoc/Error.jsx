import PropTypes from "prop-types";
import ProfileNotFoundGif from "../../assets/profile-not-found.gif";
import ServerErrorGif from "../../assets/500.gif";
import Footer from "../profile/Footer";

const ProfileNotFound = () => {
  return (
    <article className="ngnr__error__text">
      <h2 className="ngnr__error__title">Profile not found.</h2>
      <p className="ngnr__error__copy">
        We looked everywhere, but it seems that this user does not have a
        profile with us or has removed it.
      </p>
      <p className="ngnr__error__copy">
        Sorry about that! (Yes, did try rebooting.)
      </p>
    </article>
  );
};

const ServerError = () => {
  return (
    <article className="ngnr__error__text">
      <h2 className="ngnr__error__title">Internal Server Error.</h2>
      <p className="ngnr__error__copy delete-on-phone">
        We are experiencing technical difficulties server side.
      </p>
      <p className="ngnr__error__copy">
        The team has been notified and will resolve the issue shortly. Please
        try again in a few minutes.
      </p>
    </article>
  );
};

const Error = ({ status }) => {
  return (
    <section className="ngnr__error">
      <div className="ngnr__error__container">
        {status == 404 ? <ProfileNotFound /> : <ServerError />}

        <figure className="ngnr__error__gif">
          <img
            src={status == 404 ? ProfileNotFoundGif : ServerErrorGif}
            alt="not found"
          />
        </figure>
        <Footer />
      </div>
    </section>
  );
};

Error.propTypes = {
  status: PropTypes.number,
};

export default Error;
