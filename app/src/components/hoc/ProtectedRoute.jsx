import PropTypes from "prop-types";
import { Navigate } from "react-router-dom";
import AuthLoading from "../common/AuthLoading";
import Nav from "../common/Nav";
import { useAuth } from "../../hooks/useAuth.js";

const ProtectedRoute = ({ children }) => {
  const auth = useAuth();

  if (auth.isLoading) {
    return <AuthLoading />;
  }

  if (!auth.user) {
    return <Navigate to="/auth/login" />;
  }

  return (
    <div className="ngnr__protected">
      <Nav username={auth.user.username} />
      <div className="ngnr__protected__container">{children}</div>
    </div>
  );
};

ProtectedRoute.propTypes = {
  children: PropTypes.any,
};

export default ProtectedRoute;
