import PropTypes from "prop-types";
import { Helmet } from "react-helmet-async";
import Favicon from "../../assets/favicon.ico";

const WrapperSEO = ({ title, description, favicon }) => {
  const defaultTitle =
    "ngnr.club - A link-in-bio service for software developers and devops engineers.";
  const defaultDesc =
    "ngnr.club is a link-in-bio service aimed at engineers who want a simple and efficient way of gathering links to their online profiles.";
  return (
    <>
      <Helmet>
        <title>{`${title ? title + " - " : ""}${defaultTitle}`}</title>
        <meta
          name="description"
          content={`${description ? description : defaultDesc}`}
        />
        <link
          rel="shortcut icon"
          type="image/ico"
          href={favicon ? favicon : Favicon}
        />
      </Helmet>
    </>
  );
};

WrapperSEO.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  favicon: PropTypes.any,
};

export default WrapperSEO;
