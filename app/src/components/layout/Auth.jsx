import { Navigate, Outlet } from "react-router-dom";
import LogoAlt from "../../assets/LogoAlt";
import { useAuth } from "../../hooks/useAuth";
import AuthLoading from "../common/AuthLoading";

const Auth = () => {
  const auth = useAuth();
  if (auth.isLoading) {
    return <AuthLoading />;
  }

  if (auth.user) {
    return <Navigate to="/profile" />;
  } else {
    return (
      <section className="auth-page">
        <div className="auth-page__container">
          <header className="auth-page__logo">
            <LogoAlt />
          </header>
          <Outlet />
        </div>
      </section>
    );
  }
};

export default Auth;
