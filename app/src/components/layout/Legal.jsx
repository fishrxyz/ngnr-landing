import { Outlet } from "react-router-dom";
import LogoWhite from "../../assets/LogoWhite";

const Legal = () => {
  return (
    <section className="ngnr-legal">
      <section className="ngnr-legal__page">
        <div className="container">
          <Outlet />
          <footer className="ngnr-legal__footer">
            <LogoWhite />
          </footer>
        </div>
      </section>
    </section>
  );
};

export default Legal;
