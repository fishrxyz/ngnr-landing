import { RefreshDouble, SaveFloppyDisk } from "iconoir-react";
import { useContext } from "react";
import { useForm } from "react-hook-form";
import { ProfileContext } from "../../context/ProfileContext";
import FormPill from "../common/FormPill";
import Notification from "../common/Notification";

const UpdatePasswordForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data) => {
    await setNewPassword(data);
  };

  const { submitLoading, submitError, visibleSettingsForm, setNewPassword } =
    useContext(ProfileContext);

  return (
    <div
      className={`protected__form ${visibleSettingsForm === 2 && "visible"}`}
    >
      <header className="protected__form__header">
        <h3 className="protected__form__title">Password</h3>
        <p className="protected__form__subtitle">Manage your password</p>
      </header>

      <form onSubmit={handleSubmit(onSubmit)} className="protected__form__form">
        <div className="form__field">
          <label htmlFor="username" className="field__label">
            <span className="field__label__text">
              <span className={errors.old_password && "field__label--error"}>
                Old Password
              </span>
              {errors.old_password && (
                <FormPill type="error" message="required" />
              )}
            </span>

            <input
              {...register("old_password", { required: true })}
              type="password"
              name="old_password"
              className={`field__input ${errors.old_password && "error"}`}
            />
          </label>
        </div>

        <div className="form__field">
          <label htmlFor="email" className="field__label">
            <span className="field__label__text">
              <span className={errors.new_password && "field__label--error"}>
                New Password
              </span>
              {errors.new_password && (
                <FormPill type="error" message="required" />
              )}
            </span>
            <input
              {...register("new_password", { required: true })}
              type="password"
              name="new_password"
              className={`field__input ${errors.new_password && "error"}`}
            />
          </label>
        </div>

        <button
          className={`protected__form__submit ${submitLoading && "loading"}`}
        >
          {submitLoading ? (
            <>
              <RefreshDouble width={20} height={20} strokeWidth={2.2} />
              <span>Saving ...</span>
            </>
          ) : (
            <>
              <SaveFloppyDisk width={20} height={20} strokeWidth={2.2} />
              <span>Update Password</span>
            </>
          )}
        </button>
      </form>
      {submitError && <Notification error message={submitError.msg} />}
    </div>
  );
};

export default UpdatePasswordForm;
