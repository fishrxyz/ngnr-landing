import { PeaceHand } from "iconoir-react";

const Header = () => {
  return (
    <header className="settings__header protected__header">
      <h2 className="settings__greeting protected__header__greeting">
        <span>Manage your account</span>
        <PeaceHand width={35} height={35} />
      </h2>
      <p className="edit-profile__blurb protected__header__blurb">
        This is your control plane. Change your username, edit your email and
        password etc ...
      </p>
    </header>
  );
};

export default Header;
