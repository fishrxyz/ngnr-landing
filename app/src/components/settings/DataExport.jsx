import { RefreshDouble, SaveFloppyDisk } from "iconoir-react";
import { useContext } from "react";
import { ProfileContext } from "../../context/ProfileContext";
import { useAuth } from "../../hooks/useAuth";
import Notification from "../common/Notification";

const DataExport = () => {
  const { submitError, submitLoading, requestDataExport, visibleSettingsForm } =
    useContext(ProfileContext);

  const {
    user: { email },
  } = useAuth();

  const exportData = async () => {
    await requestDataExport();
  };

  return (
    <div
      className={`protected__form ${visibleSettingsForm === 4 && "visible"}`}
    >
      <header className="protected__form__header">
        <h3 className="protected__form__title">Export your data</h3>
        <p className="protected__form__subtitle">
          Request your data on ngnr.club
        </p>
      </header>

      <article className="protected__form__notice form-notice-small data-request-notice">
        <p>
          As part of our ongoing commitment to your privacy, you may request to
          download your data from ngnr.club. This data will include your profile
          and project information.
        </p>
        <p>
          Once you have requested your data, you will receive an email (at{" "}
          <strong>{email}</strong>) with a link to the archive.
        </p>
      </article>
      <button
        className={`protected__form__submit ${submitLoading && "loading"}`}
        onClick={exportData}
      >
        {submitLoading ? (
          <>
            <RefreshDouble width={20} height={20} strokeWidth={2.2} />
            <span>Sending Request ...</span>
          </>
        ) : (
          <>
            <SaveFloppyDisk width={20} height={20} strokeWidth={2.2} />
            <span>Export your data</span>
          </>
        )}
      </button>
      {submitError && <Notification error message={submitError.msg} />}
    </div>
  );
};

export default DataExport;
