import { RefreshDouble, SaveFloppyDisk } from "iconoir-react";
import { useContext } from "react";
import { useForm, useWatch } from "react-hook-form";
import { ProfileContext } from "../../context/ProfileContext";
import { useAuth } from "../../hooks/useAuth";
import FormPill from "../common/FormPill";
import Notification from "../common/Notification";

const UpdateUsernameForm = () => {
  const {
    user: { preferred_username, email },
  } = useAuth();

  const {
    register,
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: { username: preferred_username, email },
  });

  const usernameValue = useWatch({ control, name: "username" });

  const {
    submitError,
    submitLoading,
    updateGeneralSettings,
    updateProfile,
    profile,
    visibleSettingsForm,
  } = useContext(ProfileContext);

  const onSubmit = async (data) => {
    await updateGeneralSettings(data);

    // update username on the backend too
    const updatedProfile = profile;
    updatedProfile.username = data.username;
    await updateProfile(updateProfile);
  };

  return (
    <div
      className={`protected__form ${visibleSettingsForm === 1 && "visible"}`}
    >
      <header className="protected__form__header">
        <h3 className="protected__form__title">Your account information</h3>
        <p className="protected__form__subtitle">
          Update your username and / or email address
        </p>
      </header>

      <form onSubmit={handleSubmit(onSubmit)} className="protected__form__form">
        <div className="form__field">
          <label htmlFor="username" className="field__label">
            <span className="field__label__text">
              <span className={errors.username && "field__label--error"}>
                Username
              </span>
              {errors.username && <FormPill type="error" message="required" />}
            </span>

            <input
              {...register("username", { required: true })}
              type="text"
              name="username"
              className={`field__input ${errors.username && "error"}`}
            />

            <p className="field__input__data-binding">
              Your ngnr URL: ngnr.club/n/
              <strong>{usernameValue}</strong>
            </p>
          </label>
        </div>

        <div className="form__field">
          <label htmlFor="email" className="field__label">
            <span className="field__label__text">
              <span className={errors.email && "field__label--error"}>
                E-mail
              </span>
              {errors.email && <FormPill type="error" message="required" />}
            </span>
            <input
              {...register("email", { required: true })}
              type="text"
              name="email"
              className={`field__input ${errors.email && "error"}`}
            />
          </label>
        </div>

        <button
          className={`protected__form__submit ${submitLoading && "loading"}`}
        >
          {submitLoading ? (
            <>
              <RefreshDouble width={20} height={20} strokeWidth={2.2} />
              <span>Saving ...</span>
            </>
          ) : (
            <>
              <SaveFloppyDisk width={20} height={20} strokeWidth={2.2} />
              <span>Save changes</span>
            </>
          )}
        </button>
      </form>
      {submitError && <Notification error message={submitError.msg} />}
    </div>
  );
};

export default UpdateUsernameForm;
