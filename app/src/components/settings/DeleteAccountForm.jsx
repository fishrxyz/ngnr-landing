import { ArrowLeft, RefreshDouble } from "iconoir-react";
import { useContext, useEffect, useState } from "react";
import { ProfileContext } from "../../context/ProfileContext";
import DeleteAccountGif from "../../assets/delete-account.gif";
import { deleteUserAccount } from "../../helpers/auth";
import { useNavigate } from "react-router-dom";
import Notification from "../common/Notification";

const DeleteAccountForm = () => {
  const { toggleDeleteAccount } = useContext(ProfileContext);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    const close = (e) => {
      if (e.key === "Escape") {
        toggleDeleteAccount();
      }
    };
    window.addEventListener("keydown", close);
    return () => window.removeEventListener("keydown", close);
  }, [toggleDeleteAccount]);

  useEffect(() => {
    if (success) {
      navigate("/");
    }
  }, [navigate, success]);

  const confirmDeleteAccount = async () => {
    setIsLoading(true);
    setError(false);
    try {
      await deleteUserAccount();
      setSuccess(true);
    } catch (error) {
      console.log(error);
      setError({ msg: error.message });
    }

    setIsLoading(false);
  };

  return (
    <>
      {error && <Notification mini error message={error.msg} />}
      <section className="protected__modal">
        <div className="protected__modal__container">
          <button
            onClick={toggleDeleteAccount}
            className="project__modal__back-btn"
          >
            <ArrowLeft width={22} height={22} strokeWidth={3} />
          </button>
          <header className="protected__modal__header">
            <h2 className="protected__modal__title">We're sad to see you go</h2>
          </header>

          <figure className="protected__modal__image">
            <img src={DeleteAccountGif} alt="delete-account" />
          </figure>

          <article className="protected__modal__blurb">
            <p>
              If you'd like to update your password or change your username, you
              can do so in the settings page.
            </p>
            <p>
              Please be advised that <strong>account deletion is final</strong>.
              There will be <strong>no way to restore your account</strong>.
            </p>
          </article>

          <div className="protected__modal__buttons">
            <button
              onClick={toggleDeleteAccount}
              className="cancel"
              type="button"
            >
              Nevermind
            </button>
            <button
              className={`${isLoading && "loading"}`}
              onClick={confirmDeleteAccount}
              type="button"
            >
              {isLoading ? (
                <>
                  <RefreshDouble width={20} height={20} strokeWidth={2.2} />
                  <span>Deleting ...</span>
                </>
              ) : (
                <span>Delete my account</span>
              )}
            </button>
          </div>
        </div>
      </section>
    </>
  );
};

export default DeleteAccountForm;
