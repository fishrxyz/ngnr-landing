import { useContext } from "react";
import { ProfileContext } from "../../context/ProfileContext";

const ConnectedAccountsForm = () => {
  const { visibleSettingsForm } = useContext(ProfileContext);

  return (
    <div
      className={`protected__form ${visibleSettingsForm === 4 && "visible"}`}
    >
      <header className="protected__form__header">
        <h3 className="protected__form__title">External Accounts</h3>
        <p className="protected__form__subtitle">
          Connect your Github and Gitlab accounts below
        </p>
      </header>

      <article className="protected__form__notice">
        <p>
          This feature is in active development and will be part of the next
          release.
        </p>
        <p>Please check back in a few days</p>
      </article>
    </div>
  );
};

export default ConnectedAccountsForm;
