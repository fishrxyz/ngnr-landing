import { useContext } from "react";
import { Controller, useForm } from "react-hook-form";
import { ProfileContext } from "../../context/ProfileContext";
import * as Switch from "@radix-ui/react-switch";
import { RefreshDouble, SaveFloppyDisk } from "iconoir-react";

const NotificationsPreferences = () => {
  const { submitLoading, updateProfile, profile, visibleSettingsForm } =
    useContext(ProfileContext);

  const { control, handleSubmit } = useForm({
    defaultValues: profile.notifications,
  });

  const onSubmit = async (data) => {
    const updatedProfile = profile;
    updatedProfile.notifications = data;
    await updateProfile(updatedProfile);
  };

  console.log(profile.notifications);

  return (
    <div
      className={`protected__form ${visibleSettingsForm === 3 && "visible"}`}
    >
      <header className="protected__form__header">
        <h3 className="protected__form__title">Email Notifications</h3>
        <p className="protected__form__subtitle">
          Stay in the loop with what's happening at ngnr.club
        </p>
      </header>
      <form onSubmit={handleSubmit(onSubmit)} className="protected__form__form">
        <fieldset className="protected__form__group protected__form__group--options">
          <div className="form__field">
            <h4>Career / freelance opportunities</h4>
            <Controller
              control={control}
              name="jobs"
              render={({ field }) => (
                <Switch.Root
                  defaultChecked={profile.notifications.jobs}
                  name={field.name}
                  onCheckedChange={field.onChange}
                  className="toggle-switch"
                >
                  <Switch.Thumb className="toggle-switch__thumb" />
                </Switch.Root>
              )}
            />
          </div>
          <div className="form__field">
            <h4>Community spotlight</h4>
            <Controller
              control={control}
              name="community_spotlight"
              render={({ field }) => (
                <Switch.Root
                  defaultChecked={profile.notifications.community_spotlight}
                  name={field.name}
                  onCheckedChange={field.onChange}
                  className="toggle-switch"
                >
                  <Switch.Thumb className="toggle-switch__thumb" />
                </Switch.Root>
              )}
            />
          </div>
          <div className="form__field">
            <h4>Release announcements</h4>
            <Controller
              control={control}
              name="product_updates"
              render={({ field }) => (
                <Switch.Root
                  defaultChecked={profile.notifications.product_updates}
                  name={field.name}
                  onCheckedChange={field.onChange}
                  className="toggle-switch"
                >
                  <Switch.Thumb className="toggle-switch__thumb" />
                </Switch.Root>
              )}
            />
          </div>
        </fieldset>
        <button
          className={`protected__form__submit ${submitLoading && "loading"}`}
        >
          {submitLoading ? (
            <>
              <RefreshDouble width={20} height={20} strokeWidth={2.2} />
              <span>Saving ...</span>
            </>
          ) : (
            <>
              <SaveFloppyDisk width={20} height={20} strokeWidth={2.2} />
              <span>Save changes</span>
            </>
          )}
        </button>
      </form>
    </div>
  );
};

export default NotificationsPreferences;
