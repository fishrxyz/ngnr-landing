import { NavArrowDown } from "iconoir-react";
import { useContext, useState } from "react";
import { ProfileContext } from "../../context/ProfileContext";

const Nav = () => {
  const { visibleSettingsForm, toggleSettingsForm, toggleDeleteAccount } =
    useContext(ProfileContext);

  const [isOpen, setIsOpen] = useState(false);

  const buttons = [
    { name: "General", disabled: false },
    { name: "Password", disabled: false },
    { name: "Notifications", disabled: false, new: true },
    { name: "Data Export", disabled: false, new: true },
  ];

  return (
    <nav className="protected__nav settings__nav">
      <ul
        onClick={() => setIsOpen(!isOpen)}
        className={`protected__nav__links ${isOpen ? "open" : undefined}`}
      >
        <NavArrowDown
          className={`protected__nav__arrow ${isOpen ? "nav-open" : undefined}`}
          width={20}
          height={20}
          strokeWidth={2.2}
        />
        {buttons.map((btn, idx) => (
          <li
            className={`protected__nav__item ${
              visibleSettingsForm === idx + 1 ? "active" : ""
            } ${btn.className}`}
            onClick={() => toggleSettingsForm(idx + 1)}
            key={idx}
          >
            <span className="protected__nav__item__name">
              {btn.name}
              {btn.disabled && (
                <span className="protected__nav__pill soon-pill">soon</span>
              )}
              {btn.new && (
                <span className="protected__nav__pill new-pill">new</span>
              )}
            </span>
          </li>
        ))}

        <li
          onClick={toggleDeleteAccount}
          className="protected__nav__item danger"
        >
          Danger Zone
        </li>
      </ul>
    </nav>
  );
};

export default Nav;
