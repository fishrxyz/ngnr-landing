import { RefreshDouble } from "iconoir-react";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Notification from "../common/Notification";
import { useAuth } from "../../hooks/useAuth.js";

const LoginForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const navigate = useNavigate();
  const [isSubmitting, setIsSubmitting] = useState(false);

  const auth = useAuth();

  const handleSubmit = async (e) => {
    setIsSubmitting(true);
    e.preventDefault();
    setError("");

    try {
      await auth.signIn(username, password);
      navigate("/profile");
    } catch (error) {
      console.log(error.name);
      setError({ msg: error.message });
    } finally {
      setIsSubmitting(false);
    }
  };

  return (
    <>
      {error && <Notification fixed error message={error.msg} />}
      <form onSubmit={handleSubmit} className="auth__form login-form">
        <div className="form__field">
          <label htmlFor="email" className="field__label">
            <span className="field__label__text">Username</span>
            <input
              type="text"
              name="username"
              id="username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              className="field__input"
            />
          </label>
        </div>
        <div className="form__field">
          <label htmlFor="password" className="field__label">
            <span className="field__label__text">Password</span>
            <input
              type="password"
              name="password"
              id="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              className="field__input"
            />
          </label>
        </div>
        <button
          className={`auth__form__submit ${isSubmitting ? "submitting" : ""}`}
          type="submit"
        >
          <p className="auth__form__submit__text">
            {isSubmitting ? (
              <>
                <span>Logging you in</span>
                <i>
                  <RefreshDouble width="13" height="13" strokeWidth="3" />
                </i>
              </>
            ) : (
              <>
                <span>Log me in</span>
              </>
            )}
          </p>
        </button>
      </form>
      <footer className="auth__form__footer">
        <p className="auth__form__cta">
          Don't have an account ? <Link to="/auth/register">Register</Link>
        </p>
        <p className="auth__form__cta">
          Forgot your password ?{" "}
          <Link to="/auth/forgot-password">Click here</Link>
        </p>
      </footer>
    </>
  );
};

export default LoginForm;
