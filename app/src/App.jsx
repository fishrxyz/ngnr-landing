import { Route, Routes } from "react-router-dom";
import { HelmetProvider } from "react-helmet-async";
import HomePage from "./pages/home/Home";
import Profile from "./pages/profile/Profile";
import { useLocation } from "react-router-dom";
import { useEffect } from "react";
import Login from "./pages/login/Login";
import ForgotPassword from "./pages/login/ForgotPassword";
import Register from "./pages/register/Register";
import Auth from "./components/layout/Auth";
import ConfirmRegister from "./pages/register/ConfirmRegister";
import EditProfile from "./pages/edit-profile/EditProfile";
import { AuthProvider } from "./context/AuthContext";
import ProtectedRoute from "./components/hoc/ProtectedRoute";
import NotFound from "./pages/not-found/NotFound";
import { ProfileProvider } from "./context/ProfileContext";
import Legal from "./components/layout/Legal";
import TermsOfService from "./pages/Legal/Terms";
import PrivacyPolicy from "./pages/Legal/Privacy";
import CookiePolicy from "./pages/Legal/Cookies";
import FAQPage from "./pages/faq/FAQ";
import Changelog from "./pages/changelog/Changelog";
import SettingsPage from "./pages/settings/SettingsPage";
import ResetPassword from "./pages/login/ResetPassword";

const ScrollToTop = () => {
  const { pathname } = useLocation();

  useEffect(() => {
    // "document.documentElement.scrollTo" is the magic for React Router Dom v6
    document.documentElement.scrollTo({
      top: 0,
      left: 0,
      behavior: "instant", // Optional if you want to skip the scrolling animation
    });
  }, [pathname]);

  return null;
};

function App() {
  const helmetContext = {};
  return (
    <HelmetProvider context={helmetContext}>
      <ScrollToTop />
      <AuthProvider>
        <div className="app">
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route
              path="/profile"
              element={
                <ProtectedRoute>
                  <ProfileProvider>
                    <EditProfile />
                  </ProfileProvider>
                </ProtectedRoute>
              }
            />
            <Route
              path="/settings"
              element={
                <ProtectedRoute>
                  <ProfileProvider>
                    <SettingsPage />
                  </ProfileProvider>
                </ProtectedRoute>
              }
            />
            <Route path="/n/:username" element={<Profile />} />
            <Route path="/auth" element={<Auth />}>
              <Route path="login" element={<Login />} />
              <Route path="register" element={<Register />} />
              <Route path="confirm" element={<ConfirmRegister />} />
              <Route path="forgot-password" element={<ForgotPassword />} />
              <Route path="reset-password" element={<ResetPassword />} />
            </Route>
            <Route path="/legal" element={<Legal />}>
              <Route path="terms" element={<TermsOfService />} />
              <Route path="privacy" element={<PrivacyPolicy />} />
              <Route path="cookies" element={<CookiePolicy />} />
            </Route>
            <Route path="/faq" element={<FAQPage />} />
            <Route path="/changelog" element={<Changelog />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </div>
      </AuthProvider>
    </HelmetProvider>
  );
}

export default App;
