# ngnr.club landing page

Inspiration

- [univer.se](https://univer.se)
- [vercel](https://vercel.com/home)
- [hyper](https://hyper.is/)
- [carbon](https://carbon.now.sh/)
- [omg.lol](https://home.omg.lol/)

Style Guidelines

- Probably monochromatic with an accent colour in a few places
- Minimalistic
- Monospaced font with a sans serif font
- Maybe some gifs to fill in the space

Sections

- Hero (massive LOGO)
- Some fun anime GIF
- Blurb in bold and black
- How to use ?
- Features ? (now page / connect with GitHub x GitLab / all your links in one place)
- Check your username section ?
- Footer with the usual made in blah
- Ngnr.club is being built live on twitch
- questions ? e-mail us
- we'll open back up soon !
- Some GIF that says have a nice day

## Tips

### Update a user policy

```bash
aws iam create-policy-version --policy-arn $POLICY_ARN --policy-document file://path/to/policy.json --set-as-default
```
